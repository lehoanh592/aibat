import Cookies from 'js-cookie'

const state = () => {
  return {
    getLoyaltyPoint: {
      url: '/point',
      method: 'GET'
    },
    postLoyaltyPoint: {
      url: '/point',
      method: 'POST'
    }
  }
}
const actions = {
  getLoyaltyPoint(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getLoyaltyPoint.url,
      method: vueContext.state.getLoyaltyPoint.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  postLoyaltyPoint(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.postLoyaltyPoint.url,
      method: vueContext.state.postLoyaltyPoint.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  }
}
export default {
  state,
  actions
}
