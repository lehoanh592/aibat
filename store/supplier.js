import Cookies from 'js-cookie'

const state = () => {
  return {
    getListSupplier: {
      url: '/supplier',
      method: 'GET',
    },
    getListOrder: {
      url: '/order',
      method: 'GET'
    },
    getListOrderDebt: {
      url: '/report/debt',
      method: 'GET'
    },
    add: {
      url: '/supplier',
      method: 'POST',
    },
    edit: {
      url: '/supplier/$id',
      method: 'PUT',
    },
    delete: {
      url: '/supplier/$id',
      method: 'DELETE',
    },
    detail: {
      url: '/supplier/$id',
      method: 'GET',
    },
    updateDefault: {
      url: 'supplier/$id',
      method: 'PATCH',
    },
    repay: {
      url: '/supplier-debt/repay',
      method: 'POST',
    },
    supplierDebtRepayHistory:{
      url: '/supplier-debt/history',
      method: 'GET',
    },
  }
}
const actions = {
  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  supplierDebtRepayHistory(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.supplierDebtRepayHistory.url,
      method: vueContext.state.supplierDebtRepayHistory.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  repay(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.repay.url,
      method: vueContext.state.repay.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  updateDefault(vueContext, payload) {
    let url = vueContext.state.updateDefault.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.updateDefault.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getListSupplier(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListSupplier.url,
      method: vueContext.state.getListSupplier.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListOrder(vueContext, payload) {
    let url = vueContext.state.getListOrder.url
      const response = this.$axios({
        url: url,
        method: vueContext.state.getListOrder.method,
        baseURL: URL.BASE,
        headers: {
          Authorization:
            Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  getListOrderDebt(vueContext, payload) {
    let url = vueContext.state.getListOrderDebt.url
    const response = this.$axios({
      url: url,
      method: vueContext.state.getListOrderDebt.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
