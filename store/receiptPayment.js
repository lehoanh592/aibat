import Cookies from 'js-cookie'

const state = () => {
  return {
    receiptList: {
      url: '/receipt-payment/receipt',
      method: 'GET',
    },
    receiptAdd: {
      url: '/receipt-payment/receipt',
      method: 'POST',
    },
    receiptEdit: {
      url: '/receipt-payment/receipt/$id',
      method: 'PUT',
    },
    paymentAdd: {
      url: '/receipt-payment/payment',
      method: 'POST',
    },
    paymentEdit: {
      url: '/receipt-payment/payment/$id',
      method: 'PUT',
    },
    delete: {
      url: '/receipt-payment/$id',
      method: 'DELETE',
    },

  }
}
const actions = {
  receiptList(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.receiptList.url,
      method: vueContext.state.receiptList.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload)
    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
    })
    return response
  },
  receiptAdd(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.receiptAdd.url,
      method: vueContext.state.receiptAdd.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  receiptEdit(vueContext, payload) {
    let url = vueContext.state.receiptEdit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.receiptEdit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  paymentAdd(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.paymentAdd.url,
      method: vueContext.state.paymentAdd.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  paymentEdit(vueContext, payload) {
    let url = vueContext.state.paymentEdit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.paymentEdit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
