import Cookies from 'js-cookie'

const state = () => {
  return {
    getListProduct: {
      url: '/v2/product',
      method: 'GET',
    },
    getListProductV3: {
      url: '/v3/product',
      method: 'GET',
    },
    getListStorage: {
      url: '/product-storage/history-storage',
      method: 'GET',
    },
    add: {
      url: '/product',
      method: 'POST',
    },
    detail: {
      url: '/product/$id',
      method: 'GET',
    },
    edit: {
      url: '/product/$id',
      method: 'PUT',
    },
    show: {
      url: '/product/$id/show',
      method: 'post',
    },
    delete: {
      url: '/product/$id',
      method: 'DELETE',
    },
    upload: {
      url: '/uploads',
      method: 'POST',
    },
    uploadProduct: {
      method: 'POST',
    },
    importProduct: {
      url: '/product/import',
      method: 'POST',
    },
    importProductV2: {
      url: '/product/importV2',
      method: 'POST',
    },
    uploadImei: {
      url: '/product/import-imei/$id',
      method: 'POST',
    },
    changePrice: {
      url: '/product/change-price',
      method: 'POST',
    },
    deleteMultipleProducts: {
      url: '/product/delete-list',
      method: 'DELETE',
    },
  }
}
const actions = {
  deleteMultipleProducts(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.deleteMultipleProducts.url,
      method: vueContext.state.deleteMultipleProducts.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      }
    })
    return response
  },
  getListStorage(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListStorage.url,
      method: vueContext.state.getListStorage.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListProduct(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListProduct.url,
      method: vueContext.state.getListProduct.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListProductV3(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListProductV3.url,
      method: vueContext.state.getListProductV3.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  show(vueContext, payload) {
    let url = vueContext.state.show.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.show.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  async upload(vueContext, payload) {
    const respone = await this.$axios({
      url: 'https://cdn-img.aibat.vn/api/uploads',
      method: vueContext.state.upload.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload,
    })
    return respone
  },
  async uploadProduct(vueContext, payload) {
    const respone = await this.$axios({
      url: 'https://cdn-img.aibat.vn/api/uploads/product',
      method: vueContext.state.uploadProduct.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload,
    })
    return respone
  },
  changePrice(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.changePrice.url,
      method: vueContext.state.changePrice.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  async importProduct(vueContext, payload) {
    const respone = await this.$axios({
      url: vueContext.state.importProduct.url,
      method: vueContext.state.importProduct.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return respone
  },
  importProductV2(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.importProductV2.url,
      method: vueContext.state.importProductV2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  async uploadImei(vueContext, data) {
    let url = 'https://cdn-img.aibat.vn/api/uploads'
    url = url.replace('$id', data.id)

    const respone = await this.$axios({
      url: url,
      method: vueContext.state.uploadImei.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {},
      data: data.data,
    })
    return respone
  },

}

export default {
  state,
  actions,
}
