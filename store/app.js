import Cookies from 'js-cookie'
import ULT from '~/plugins/ult'
import APIs from '~/assets/configurations/API_Config'
import moment from 'moment'
import constant from '~/assets/configurations/constant.js'
import Constants from '~/assets/configurations/constant.js'
import ConstantsValue from '~/assets/configurations/constValue.js'

export let state = () => ({
  ip: '',
  roles: [],
  getContent: '',
  pageSizes: [10, 20, 50, 100, 200],
  getRoles: false,
  requestObjectType: null,
  onlyMe: true,
  alert_display_time: 6000,
  stateOnlye: true,
  dateTimeDashboard: [
    moment().startOf('year').format('YYYY-MM-DD'),
    moment().format('YYYY-MM-DD')
  ],
  changePass: {
    url: '/user/change-password',
    method: 'POST'
  },
  strRoles: null
  // reload_treeview: false,
})

export const mutations = {
  ClientPrivateIP(state, ip) {
    state.ip = ip
  },
  set_time_alert(state, payload) {
    state.alert_display_time = Number(payload)
  },
  setRole(state, payload) {
    state.roles = payload
    state.getRoles = true
  },
  setRequestObjectType(state, payload) {
    state.requestObjectType = payload
  },
  setDateTimeDashboard(state, payload) {
    state.dateTimeDashboard = payload
  },
  setOnlyme(state, payload) {
    state.onlyMe = payload
  },
  setStateOnlyme(state, payload) {
    state.stateOnlye = payload
  },
  setContent(state, payload) {
    state.getContent = payload
  }
}

export const actions = {
  async changePass(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.changePass.url,
      method: vueContext.state.changePass.method,
      headers: {
        Authorization: `Bearer ${Cookies.get('token')}`
      },
      data: {
        ...payload
      }
    })
    return response
  },

  async GetRole(vueContext, payload) {

    await this.$getRoleByUser(payload).then((response) => {
      //list id ngành nghề
      //1: Bi-a
      //2: Quản lý nhà hàng, quán an
      //3: Quản lý bar/karaoke
      //4: Quản lý nhà nghỉ, homestay,
      //5: Quản lý khách sạn
      //6: Quản lý cửa hàng điện thoại
      //7: Quản lý bán hàng
      //9: Quản lý xưởng sản suất
      //10: Quản lý và số hoá doanh nghiệp
      //11: Quản lý cửa hàng tạp hoá
      //12: Quản lý quán cafe, đồ uống
      //17: Quản lý siêu thị
      //19: Quản lý spa, nail, massage
      //20: Quản lý kinh doanh khác
      //21: Quản lý sân bóng
      try {
        var strPermission = `${Cookies.get('strPermission')}`
        var userActiveCode = `${Cookies.get('userActiveCode')}`
        var businessId = `${Cookies.get('businessId')}`
        var userType = `${Cookies.get('type')}`
        let stt = 0
        if (businessId <= 7) {
          stt = businessId - 1
        } else if (businessId >= 9 && businessId <= 12) {
          stt = businessId - 2
        } else if (businessId > 12 && businessId <= 17) {
          stt = 11
        }
        else {
          stt = businessId - 7
        }
        let dataSidebar = Constants.System.Business[stt]

        let listUserShow = ConstantsValue.listUserShow
        let userId = `${Cookies.get('userId')}`
        let managementPermission = Cookies.get('is_management') === 'true';

        let findUserShow = listUserShow.find((i) => i.id === parseInt(userId))

        let pageRoles = [
          {
            id: 10,
            pageName: dataSidebar[0].dashboard.name,
            pageUrl: `${dataSidebar[0].dashboard.url}`,
            pageIcon: dataSidebar[0].dashboard.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].dashboard.permission) === true && dataSidebar[0].dashboard.name !== null ? true : false
          },
          {
            id: 20,
            pageName: dataSidebar[0].orderChildren.name,
            pageUrl: `${dataSidebar[0].orderChildren.url}`,
            pageIcon: dataSidebar[0].orderChildren.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].orderChildren.permission1) === true && strPermission.includes(dataSidebar[0].orderChildren.permission2) === true && dataSidebar[0].orderChildren.name !== null ? true : false
          },
          {
            id: 30,
            pageName: dataSidebar[0].order.name,
            pageUrl: `${dataSidebar[0].order.url}`,
            pageIcon: dataSidebar[0].order.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].order.permission) === true && dataSidebar[0].order.name !== null ? true : false
          },
          {
            id: 31,
            pageName: dataSidebar[0].orderChildren1.name,
            pageUrl: `${dataSidebar[0].orderChildren1.url}`,
            pageIcon: dataSidebar[0].orderChildren1.icon,
            parentId: 30,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].orderChildren1.permission) === true && dataSidebar[0].orderChildren1.name !== null ? true : false
          },
          {
            id: 32,
            pageName: dataSidebar[0].orderChildren3.name,
            pageUrl: `${dataSidebar[0].orderChildren3.url}`,
            pageIcon: dataSidebar[0].orderChildren3.icon,
            parentId: 30,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].orderChildren3.permission1) === true && strPermission.includes(dataSidebar[0].orderChildren3.permission2) === true && dataSidebar[0].orderChildren3.name !== null ? true : false
          },
          {
            id: 40,
            pageName: dataSidebar[0].order1.name,
            pageUrl: `${dataSidebar[0].order1.url}`,
            pageIcon: dataSidebar[0].order1.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].order1.permission) === true && dataSidebar[0].order.name !== null ? true : false
          },
          {
            id: 41,
            pageName: dataSidebar[0].orderChildren2.name,
            pageUrl: `${dataSidebar[0].orderChildren2.url}`,
            pageIcon: dataSidebar[0].orderChildren2.icon,
            parentId: 40,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].orderChildren2.permission) === true && dataSidebar[0].orderChildren2.name !== null ? true : false
          },
          {
            id: 42,
            pageName: dataSidebar[0].orderChildren4.name,
            pageUrl: `${dataSidebar[0].orderChildren4.url}`,
            pageIcon: dataSidebar[0].orderChildren4.icon,
            parentId: 40,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].orderChildren4.permission) === true && dataSidebar[0].orderChildren4.name !== null ? true : false
          },
          {
            id: 50,
            pageName: dataSidebar[0].product.name,
            pageUrl: `${dataSidebar[0].product.url}`,
            pageIcon: dataSidebar[0].product.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 51,
            pageName: dataSidebar[0].productChildren1.name,
            pageUrl: `${dataSidebar[0].productChildren1.url}`,
            pageIcon: dataSidebar[0].productChildren1.icon,
            parentId: 50,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].productChildren1.permission) === true && dataSidebar[0].productChildren1.name !== null ? true : false
          },

          {
            id: 52,
            pageName: dataSidebar[0].productChildren3.name,
            pageUrl: `${dataSidebar[0].productChildren3.url}`,
            pageIcon: dataSidebar[0].productChildren3.icon,
            parentId: 50,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].productChildren3.permission) === true && dataSidebar[0].productChildren3.name !== null ? true : false
          },
          {
            id: 53,
            pageName: dataSidebar[0].productChildren4.name,
            pageUrl: `${dataSidebar[0].productChildren4.url}`,
            pageIcon: dataSidebar[0].productChildren4.icon,
            parentId: 50,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].productChildren4.permission) === true && dataSidebar[0].productChildren4.name !== null ? true : false
          },
          {
            id: 54,
            pageName: dataSidebar[0].productChildren5.name,
            pageUrl: `${dataSidebar[0].productChildren5.url}`,
            pageIcon: dataSidebar[0].productChildren5.icon,
            parentId: 50,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].productChildren5.permission) === true && dataSidebar[0].productChildren5.name !== null ? true : false
          },
          {
            id: 60,
            pageName: dataSidebar[0].inventory.name,
            pageUrl: `${dataSidebar[0].inventory.url}`,
            pageIcon: dataSidebar[0].inventory.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].inventory.permission) === true && dataSidebar[0].inventory.name !== null ? true : false
          },
          {
            id: 61,
            pageName: dataSidebar[0].inventoryChildren1.name,
            pageUrl: `${dataSidebar[0].inventoryChildren1.url}`,
            pageIcon: dataSidebar[0].inventoryChildren1.icon,
            parentId: 60,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].inventoryChildren1.permission) === true && dataSidebar[0].inventoryChildren1.name !== null ? true : false
          },
          {
            id: 62,
            pageName: dataSidebar[0].inventoryChildren2.name,
            pageUrl: `${dataSidebar[0].inventoryChildren2.url}`,
            pageIcon: dataSidebar[0].inventoryChildren2.icon,
            parentId: 60,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].inventoryChildren2.permission) === true ? true : false
          },
          {
            id: 70,
            pageName: dataSidebar[0].payment.name,
            pageUrl: `${dataSidebar[0].payment.url}`,
            pageIcon: dataSidebar[0].payment.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 71,
            pageName: dataSidebar[0].paymentChildren1.name,
            pageUrl: `${dataSidebar[0].paymentChildren1.url}`,
            pageIcon: dataSidebar[0].paymentChildren1.icon,
            parentId: 70,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].paymentChildren1.permission) === true && dataSidebar[0].paymentChildren1.name !== null ? true : false
          },
          {
            id: 72,
            pageName: dataSidebar[0].paymentChildren2.name,
            pageUrl: `${dataSidebar[0].paymentChildren2.url}`,
            pageIcon: dataSidebar[0].paymentChildren2.icon,
            parentId: 70,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].paymentChildren2.permission) === true && dataSidebar[0].paymentChildren2.name !== null

          },
          {
            id: 80,
            pageName: dataSidebar[0].assigningTask.name,
            pageUrl: `${dataSidebar[0].assigningTask.url}`,
            pageIcon: dataSidebar[0].assigningTask.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].assigningTask.permission) === true && dataSidebar[0].assigningTask.name !== null && (userActiveCode !== 'FREE' && userActiveCode !== 'A39')
          },
          {
            id: 90,
            pageName: dataSidebar[0].branch.name,
            pageUrl: `${dataSidebar[0].branch.url}`,
            pageIcon: dataSidebar[0].branch.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].branch.permission) === true && dataSidebar[0].branch.name !== null ? true : false

          },
          {
            id: 100,
            pageName: dataSidebar[0].supplier.name,
            pageUrl: `${dataSidebar[0].supplier.url}`,
            pageIcon: dataSidebar[0].supplier.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission:  strPermission.includes(dataSidebar[0].supplier.permission) === true && dataSidebar[0].supplier.name !== null ? true : false
          },
          {
            id: 101,
            pageName: dataSidebar[0].productChildren2.name,
            pageUrl: `${dataSidebar[0].productChildren2.url}`,
            pageIcon: dataSidebar[0].productChildren2.icon,
            parentId: 100,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].productChildren2.permission) === true && dataSidebar[0].productChildren2.name !== null ? true : false
          },
          {
            id: 102,
            pageName: dataSidebar[0].reportChildren8.name,
            pageUrl: `${dataSidebar[0].reportChildren8.url}`,
            pageIcon: dataSidebar[0].reportChildren8.icon,
            parentId: 100,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren8.permission) === true && dataSidebar[0].reportChildren8.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39' ? true : false
          },
          {
            id: 110,
            pageName: dataSidebar[0].customer.name,
            pageUrl: `${dataSidebar[0].customer.url}`,
            pageIcon: dataSidebar[0].customer.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission:  strPermission.includes(dataSidebar[0].customer.permission) === true && dataSidebar[0].customer.name !== null ? true : false
          },
          {
            id: 111,
            pageName: dataSidebar[0].customerChildren1.name,
            pageUrl: `${dataSidebar[0].customerChildren1.url}`,
            pageIcon: dataSidebar[0].customerChildren1.icon,
            parentId: 110,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].customerChildren1.permission) === true && dataSidebar[0].customerChildren1.name !== null ? true : false
          },
          {
            id: 112,
            pageName: dataSidebar[0].reportChildren9.name,
            pageUrl: `${dataSidebar[0].reportChildren9.url}`,
            pageIcon: dataSidebar[0].reportChildren9.icon,
            parentId: 110,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren9.permission) === true && dataSidebar[0].reportChildren9.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39' ? true : false
          },
          {
            id: 120,
            pageName: dataSidebar[0].employee.name,
            pageUrl: `${dataSidebar[0].employee.url}`,
            pageIcon: dataSidebar[0].employee.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].employee.permission) === true && dataSidebar[0].employee.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39'
          },
          {
            id: 121,
            pageName: dataSidebar[0].employeeChildren1.name,
            pageUrl: `${dataSidebar[0].employeeChildren1.url}`,
            pageIcon: dataSidebar[0].employeeChildren1.icon,
            parentId: 120,
            checkPermission:  strPermission.includes(dataSidebar[0].employee.permission) === true && dataSidebar[0].employee.name !== null ? true : false
          },
          {
            id: 122,
            pageName: dataSidebar[0].employeeChildren2.name,
            pageUrl: `${dataSidebar[0].employeeChildren2.url}`,
            pageIcon: dataSidebar[0].employeeChildren2.icon,
            parentId: 120,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].employee.permission) === true && dataSidebar[0].employee.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39' ? true : false
          },
          {
            id: 123,
            pageName: dataSidebar[0].employeeChildren3.name,
            pageUrl: `${dataSidebar[0].employeeChildren3.url}`,
            pageIcon: dataSidebar[0].employeeChildren3.icon,
            parentId: 120,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].employee.permission) === true && dataSidebar[0].employee.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39' ? true : false
          },
          {
            id: 124,
            pageName: dataSidebar[0].reportChildren7.name,
            pageUrl: `${dataSidebar[0].reportChildren7.url}`,
            pageIcon: dataSidebar[0].reportChildren7.icon,
            parentId: 120,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren7.permission) === true && dataSidebar[0].reportChildren7.name !== null && userActiveCode !== 'FREE' && userActiveCode !== 'A39' ? true : false
          },
          // {
          //   id: 50,
          //   pageName: 'Phòng/bàn',
          //   pageUrl: '/phong-ban',
          //   pageIcon: 'mdi-coffee',
          //   parentId: 0,
          //   menuIndex: 3,
          //   level: 0,
          //   pageType: 0,
          //   redirectType: null,
          //   roles: '8,9,10,11,12',
          //   showOnMobile: 1,
          //   showOnTablet: 1,
          //   isOpen: null,
          //   checkPermission: strPermission.includes(',list_area,') === true && strPermission.includes(',list_room,') === true ? true : false
          // },
          // {
          //   id: 10,
          //   pageName: 'Thông tin cửa hàng',
          //   pageUrl: '/cai-dat/cua-hang',
          //   pageIcon: 'mdi-cog-outline',
          //   parentId: 9,
          //   menuIndex: 3,
          //   level: 0,
          //   pageType: 0,
          //   redirectType: null,
          //   roles: '8,9,10,11,12',
          //   showOnMobile: 1,
          //   showOnTablet: 1,
          //   isOpen: null,
          //   checkPermission: true
          // },
          {
            id: 190,
            pageName: dataSidebar[0].workSchedule.name,
            pageUrl: `${dataSidebar[0].workSchedule.url}`,
            pageIcon: dataSidebar[0].workSchedule.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: userType == 3 && !managementPermission
          },
          {
            id: 170,
            pageName: dataSidebar[0].calendar.name,
            pageUrl: `${dataSidebar[0].calendar.url}`,
            pageIcon: dataSidebar[0].calendar.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].calendar.permission) === true && dataSidebar[0].calendar.name !== null ? true : false

          },
          {
            id: 180,
            pageName: dataSidebar[0].rewardPenalty.name,
            pageUrl: `${dataSidebar[0].rewardPenalty.url}`,
            pageIcon: dataSidebar[0].rewardPenalty.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].rewardPenalty.permission) === true && dataSidebar[0].rewardPenalty.name !== null ? true : false

          },
          {
            id: 130,
            pageName: dataSidebar[0].report.name,
            pageUrl: `${dataSidebar[0].report.url}`,
            pageIcon: dataSidebar[0].report.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 131,
            pageName: dataSidebar[0].reportChildren1.name,
            pageUrl: `${dataSidebar[0].reportChildren1.url}`,
            pageIcon: dataSidebar[0].reportChildren1.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren1.permission) === true && dataSidebar[0].reportChildren1.name !== null ? true : false
          },
          {
            id: 132,
            pageName: dataSidebar[0].reportChildren2.name,
            pageUrl: `${dataSidebar[0].reportChildren2.url}`,
            pageIcon: dataSidebar[0].reportChildren2.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren2.permission) === true && dataSidebar[0].reportChildren2.name !== null ? true : false
          },
          {
            id: 138,
            pageName: dataSidebar[0].reportChildren11.name,
            pageUrl: `${dataSidebar[0].reportChildren11.url}`,
            pageIcon: dataSidebar[0].reportChildren11.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren11.permission) === true && dataSidebar[0].reportChildren11.name !== null ? true : false
          },
          {
            id: 139,
            pageName: dataSidebar[0].reportChildren12.name,
            pageUrl: `${dataSidebar[0].reportChildren12.url}`,
            pageIcon: dataSidebar[0].reportChildren12.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren12.permission) === true && dataSidebar[0].reportChildren12.name !== null
          },
          {
            id: 133,
            pageName: dataSidebar[0].reportChildren3.name,
            pageUrl: `${dataSidebar[0].reportChildren3.url}`,
            pageIcon: dataSidebar[0].reportChildren3.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren3.permission) === true && dataSidebar[0].reportChildren3.name !== null ? true : false
          },
          {
            id: 134,
            pageName: dataSidebar[0].reportChildren4.name,
            pageUrl: `${dataSidebar[0].reportChildren4.url}`,
            pageIcon: dataSidebar[0].reportChildren4.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren4.permission) === true && dataSidebar[0].reportChildren4.name !== null ? true : false
          },
          {
            id: 135,
            pageName: dataSidebar[0].reportChildren5.name,
            pageUrl: `${dataSidebar[0].reportChildren5.url}`,
            pageIcon: dataSidebar[0].reportChildren5.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren5.permission) === true && dataSidebar[0].reportChildren5.name !== null
          },
          {
            id: 136,
            pageName: dataSidebar[0].reportChildren10.name,
            pageUrl: `${dataSidebar[0].reportChildren10.url}`,
            pageIcon: dataSidebar[0].reportChildren10.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren10.permission) === true && dataSidebar[0].reportChildren10.name !== null
          },
          {
            id: 137,
            pageName: dataSidebar[0].reportChildren6.name,
            pageUrl: `${dataSidebar[0].reportChildren6.url}`,
            pageIcon: dataSidebar[0].reportChildren6.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren6.permission) === true && dataSidebar[0].reportChildren6.name !== null ? true : false
          },
          {
            id: 139,
            pageName: dataSidebar[0].reportChildren14.name,
            pageUrl: `${dataSidebar[0].reportChildren14.url}`,
            pageIcon: dataSidebar[0].reportChildren14.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren14.permission) === true && dataSidebar[0].reportChildren14.name !== null ? true : false
          },
          {
            id: 138,
            pageName: dataSidebar[0].reportChildren15.name,
            pageUrl: `${dataSidebar[0].reportChildren15.url}`,
            pageIcon: dataSidebar[0].reportChildren15.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren15.permission) === true && dataSidebar[0].reportChildren15.name !== null ? true : false
          },
          {
            id: 140,
            pageName: dataSidebar[0].zalo.name,
            pageUrl: `${dataSidebar[0].zalo.url}`,
            pageIcon: dataSidebar[0].zalo.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: (userActiveCode === 'TRIAL' || userActiveCode === 'A199')
          },
          {
            id: 150,
            pageName: dataSidebar[0].website.name,
            pageUrl: `${dataSidebar[0].website.url}`,
            pageIcon: dataSidebar[0].website.icon,
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: (userActiveCode === 'TRIAL' || userActiveCode === 'A199')
          }
        ]
        let listBusinessId = ['1', '2', '3', '4', '5', '12', '21']
        if (listBusinessId.includes(businessId)) {
          pageRoles[3].parentId = 0
          pageRoles.splice(2, 1)
          pageRoles.forEach(item =>{
            if(item.id === 138){
              item.checkPermission = false;
            }
          })
        }
        let listBusinessId1 = ['3','4','5','6', '7', '9', '10' ,'11', '17', '20',  ]
        if(listBusinessId1.includes(businessId)){
          pageRoles[10].parentId = 0
          pageRoles[12].parentId =0
        }
        let listBusinessId2 = ['1','2','3','4','12','21']
        if (listBusinessId2.includes(businessId)) {
          pageRoles.push({
            id: 1310,
            pageName: dataSidebar[0].reportChildren13.name,
            pageUrl: `${dataSidebar[0].reportChildren13.url}`,
            pageIcon: dataSidebar[0].reportChildren13.icon,
            parentId: 130,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].reportChildren13.permission) === true && dataSidebar[0].reportChildren13.name !== null
          })
        }
        if(listBusinessId2.includes(businessId)){
          pageRoles[9].parentId = 0
        }


        pageRoles.push(
          {
            id: 160,
            pageName: 'Cấu hình',
            pageUrl: '',
            pageIcon: 'mdi-cog',
            parentId: 0,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 161,
            pageName: 'Cài đặt thông tin hóa đơn',
            pageUrl: '/cai-dat/cua-hang',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 162,
            pageName: 'Cài đặt đơn bán',
            pageUrl: '/cai-dat/cai-dat-don-ban',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 163,
            pageName: 'Cài đặt mẫu hóa đơn',
            pageUrl: '/cai-dat/cai-dat-hoa-don',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: true
          },
          {
            id: 164,
            pageName: 'Tích điểm',
            pageUrl: '/cai-dat/tich-diem',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].employee.permission)
          },
          {
            id: 165,
            pageName: 'Liên kết thanh toán',
            pageUrl: '/cai-dat/lien-ket-thanh-toan',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(dataSidebar[0].employee.permission)
          },
          {
            id: 166,
            pageName: 'Thông tin quy định',
            pageUrl: '/quy-dinh',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: strPermission.includes(',list_rules,')
          },
          {
            id: 167,
            pageName: 'Khuyến mãi',
            pageUrl: '/cai-dat/khuyen-mai',
            pageIcon: 'mdi-minus-thick',
            parentId: 160,
            menuIndex: 3,
            level: 0,
            pageType: 0,
            redirectType: null,
            roles: '8,9,10,11,12',
            showOnMobile: 1,
            showOnTablet: 1,
            isOpen: null,
            checkPermission: businessId === '7'
          }
        )
        vueContext.commit('setRole', pageRoles)

        let listRoles = []
        for (let i = 0; i < pageRoles.length; i++) {
          listRoles.push(pageRoles[i].roles)
        }
        let str = ',' + listRoles.toString() + ','
        Cookies.set('strRoles', str, { expires: 24 * 30 * 12 })
      } catch (e) {
      }
    })
  }
}

export const getters = {}
