import Cookies from 'js-cookie'

const state = () => {
  return {
    getListBrand: {
      url: '/brand',
      method: 'GET',
    },
    add: {
      url: '/brand',
      method: 'POST',
    },
    edit: {
      url: '/brand/$id',
      method: 'PUT',
    },
    delete: {
      url: '/brand/$id',
      method: 'DELETE',
    },
  }
}
const actions = {
  getListBrand(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListBrand.url,
      method: vueContext.state.getListBrand.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
