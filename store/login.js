import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config'

export const state = () => ({
  selectedRole: {
    value: ''
  },
  phone: {
    errors: [],
    value: ''
  },
  password: {
    errors: [],
    value: ''
  },
  path: null,
  isLogin: false,
  logging: false
})

export const mutations = {
  setPhone(state, payload) {
    state.phone = {
      ...state.phone,
      value: payload,
      errors: []
    }
  },
  setPath(state, payload) {
    state.path = payload
  },
  setSelectedRole(state, payload) {
    state.selectedRole.value = payload
  },
  setLogging(state, payload) {
    state.logging == payload
  },
  unsetItem(state) {
    state.selectedRole.value = ''
    state.phone.value = ''
    state.password.value = ''
  },
  setPassword(state, payload) {
    state.password = {
      ...state.password,
      value: payload,
      errors: []
    }
  },
  phoneHasErrors(state, payload) {
    state.phone = {
      ...state.phone,
      errors: [payload]
    }
  },
  passwordHasErrors(state, payload) {
    state.password = {
      ...state.password,
      errors: [payload]
    }
  },
  setIsLogin(state, payload) {
    state.isLogin = payload
  },
  clearErrors(state, payload) {
    state.phone = {
      ...state.phone,
      errors: []
    }
    state.password = {
      ...state.password,
      errors: []
    }

  }
}

export const actions = {
  isAuthenticated(vueContext, req) {
    if (req) {
      let token
      try {
        token = req.headers.cookie
          .split(';')
          .find((c) => c.trim().startsWith('token='))
          .split('=')[1]
      } catch (e) {
        token = ''
      }
      return token
    } else {
      const token =
        Cookies.get('token') !== undefined ? Cookies.get('token') : ''
      return token
    }
  },
  setPhone(vueContext, payload) {
    vueContext.commit('setPhone', payload)
  },
  setSelectedRole(vueContext, payload) {
    vueContext.commit('setSelectedRole', payload)
  },
  setPassword(vueContext, payload) {
    vueContext.commit('setPassword', payload)
  },

  forgotPass(vueContext, payload) {
    const response = this.$axios({
      url: '/user/reset-password',
      method: 'POST',
      baseURL: URL.BASE,
      data: {
        ...payload
      }
    })
    return response
  },
  forgotPassConfirm(vueContext, payload) {
    const response = this.$axios({
      url: '/user/reset-password-confirm',
      method: 'POST',
      baseURL: URL.BASE,
      data: {
        ...payload
      }
    })
    return response
  },
  forgotPassSuccess(vueContext, payload) {
    const response = this.$axios({
      url: '/user/reset-password-success',
      method: 'POST',
      baseURL: URL.BASE,
      data: {
        ...payload
      }
    })
    return response
  },

  Submit(vueContext) {
    const selectedRole = vueContext.state.selectedRole.value
    const phone = vueContext.state.phone.value
    const password = vueContext.state.password.value
    let type;
    if (selectedRole == 1) {
      type = 3;
    } else {
      type = 2;
    }
    vueContext.commit('clearErrors')
    let payload = {
      type: type,
      phone: phone,
      password: password
    }
    const response = this.$axios({
      url: '/login',
      method: 'POST',
      baseURL: URL.BASE,
      data: {
        ...payload
      }
    })
    vueContext.commit('unsetItem')
    return response
  }
}

export const getters = {}
