import Cookies from 'js-cookie'

const state = () => {
  return {
    getTaskList: {
      url: '/task',
      method: 'GET',
    },
    addTask: {
      url: '/task',
      method: 'POST',
    },
    updateTask: {
      url: '/task/$id',
      method: 'PUT',
    },
    uploadFile: {
      url: '/uploads/file',
      method: 'POST',
    },

  }
}

const actions = {
  getTaskList(VueContext, payload) {
    const response = this.$axios({
      url: VueContext.state.getTaskList.url,
      method: VueContext.state.getTaskList.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  addTask(VueContext, payload){
    const response = this.$axios({
      url: VueContext.state.addTask.url,
      method: VueContext.state.addTask.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload
      }
    })
    return response
  },
  updateTask(VueContext, payload){
    const reponse = this.$axios({
      url: VueContext.state.updateTask.url.replace('$id', payload.id),
      method: VueContext.state.updateTask.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload
      }
    })
    return reponse
  },
  async uploadFile(VueContext, payload){
    const response = await this.$axios({
      url: 'https://cdn-img.aibat.vn/api/uploads',
      method: VueContext.state.uploadFile.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload
    })
    return response
  }
}
export default {  state,  actions}
