import Cookies from 'js-cookie'

const state = () => {
  return {
    add: {
      url: '/calendar',
      method: 'POST',
    },
    getListCalender: {
      url: '/calendar',
      method: 'GET',
    },
    detail: {
      url: '/calendar/detail',
      method: 'GET',
    },
    edit:{
      url: '/calendar/$id',
      method: 'PUT',
    },
    delete:{
      url: '/calendar/$id',
      method: 'DELETE',
    },
    manyDelete:{
      url: '/calendar/delete-many',
      method: 'DELETE',
    },
  }
}
const actions = {
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getListCalender(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListCalender.url,
      method: vueContext.state.getListCalender.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  detail(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.detail.url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  deleteMany(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.deleteMany.url,
      method: vueContext.state.deleteMany.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
