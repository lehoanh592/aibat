import Cookies from 'js-cookie'

const state = () => {
  return {
    getListArea: {
      url: '/area',
      method: 'GET',
    },
    add: {
      url: '/area',
      method: 'POST',
    },
    addMultiple: {
      url: '/area/list',
      method: 'POST',
    },
    edit: {
      url: '/area/$id',
      method: 'PUT',
    },
    delete: {
      url: '/area/$id',
      method: 'DELETE',
    },
  }
}
const actions = {
  getListArea(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListArea.url,
      method: vueContext.state.getListArea.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  addMultiple(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addMultiple.url,
      method: vueContext.state.addMultiple.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
