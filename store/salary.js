import Cookies from 'js-cookie'

const state = () => {
  return {
    add: {
      url: '/salary',
      method: 'POST',
    },
    edit: {
      url: '/salary/$id',
      method: 'PUT',
    },
    monthlySalary: {
      url: '/monthly-salary',
      method: 'GET',
    },
  }
}
const actions = {

  monthlySalary(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.monthlySalary.url,
      method: vueContext.state.monthlySalary.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListOvertime(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListOvertime.url,
      method: vueContext.state.getListOvertime.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.user_id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

}

export default {
  state,
  actions,
}
