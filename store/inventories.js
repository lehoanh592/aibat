import Cookies from 'js-cookie'

const state = () => {
  return {
    getListInvertories:{
      url:'/inventories',
      method: 'GET'
    },
    getListWarehouseBalance: {
      url: '/inventories/$id',
      method: 'PUT',
    },
    getInventoryCheckSheet:{
      url: '/inventories/',
      method: 'POST',
    },
    delete: {
      url: '/inventories/$id',
      method: 'DELETE',
    },
    updateBaseCost: {
      url: '/inventories/$id/cost',
      method: 'PUT',
    },

  }
}
const actions = {
  getListInvertories(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListInvertories.url,
      method: vueContext.state.getListInvertories.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  updateBaseCost(vueContext, payload) {
    let url = vueContext.state.updateBaseCost.url
    url = vueContext.state.updateBaseCost.url.replaceAll('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.updateBaseCost.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getListWarehouseBalance(vueContext, payload) {
    let url = vueContext.state.getListWarehouseBalance.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.getListWarehouseBalance.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getInventoryCheckSheet(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getInventoryCheckSheet.url,
      method: vueContext.state.getInventoryCheckSheet.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept' : 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
