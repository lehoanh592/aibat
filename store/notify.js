import Cookies from 'js-cookie'

const state = () => {
  return {
    getListUserNotify: {
      url: '/user/notify',
      method: 'GET',
    },
    readNotify: {
      url: '/notify',
      method: 'PUT',
    },
  }
}
const actions = {
  getListUserNotify(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListUserNotify.url,
      method: vueContext.state.getListUserNotify.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  readNotify(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.readNotify.url,
      method: vueContext.state.readNotify.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },


}

export default {
  state,
  actions,
}
