import Cookies from 'js-cookie'

const state = () => {
  return {
    update: {
      url: '/user',
      method: 'POST',
    },
    userInfo: {
      url: '/user/info',
      method: 'GET',
    },
  }
}
const actions = {
  update(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.update.url,
      method: vueContext.state.update.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : `Bearer ${Cookies.get('token_backup')}`,
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  userInfo(vueContext) {
    const response = this.$axios({
      url: vueContext.state.userInfo.url,
      method: vueContext.state.userInfo.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : `Bearer ${Cookies.get('token_backup')}`,
      },
    })
    return response
  }
}

export default {
  state,
  actions,
}
