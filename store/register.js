import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config'

export const state = () => ({
  phone: {
    errors: [],
    value: ''
  },
  password: {
    errors: [],
    value: ''
  },
  passwordConfirm: {
    errors: [],
    value: ''
  },
  referral: {
    errors: [],
    value: ''
  },
  retryOtp: {
    url: '/otp',
    method: 'POST'
  },
  submitOtp: {
    url: '/user/active',
    method: 'POST'
  },
  registerAction: {
    url: '/register',
    method: 'POST'
  },
  path: null,
  isLogin: false,
  logging: false
})

export const mutations = {
  setPhone(state, payload) {
    state.phone = {
      ...state.phone,
      value: payload,
      errors: []
    }
  },
  setPath(state, payload) {
    state.path = payload
  },
  setLogging(state, payload) {
    state.logging == payload
  },
  unsetItem(state) {
    state.phone.value = ''
    state.password.value = ''
    state.passwordConfirm.value = ''
    state.referral.value = ''
  },
  setPassword(state, payload) {
    state.password = {
      ...state.password,
      value: payload,
      errors: []
    }
  },
  setPasswordConfirm(state, payload) {
    state.passwordConfirm = {
      ...state.passwordConfirm,
      value: payload,
      errors: []
    }
  },
  setReferral(state, payload) {
    state.referral = {
      ...state.referral,
      value: payload,
      errors: []
    }
  },
  phoneHasErrors(state, payload) {
    state.phone = {
      ...state.phone,
      errors: [payload]
    }
  },
  passwordHasErrors(state, payload) {
    state.password = {
      ...state.password,
      errors: [payload]
    }
  },
  passwordConfirmHasErrors(state, payload) {
    state.passwordConfirm = {
      ...state.passwordConfirm,
      errors: [payload]
    }
  },
  referralHasErrors(state, payload) {
    state.referral = {
      ...state.referral,
      errors: [payload]
    }
  },
  setIsLogin(state, payload) {
    state.isLogin = payload
  },
  clearErrors(state, payload) {
    state.phone = {
      ...state.phone,
      errors: []
    }
    state.password = {
      ...state.password,
      errors: []
    }
  }
}

export const actions = {
  isAuthenticated(vueContext, req) {
    if (req) {
      let token
      try {
        token = req.headers.cookie
          .split(';')
          .find((c) => c.trim().startsWith('token='))
          .split('=')[1]
      } catch (e) {
        token = ''
      }
      return token
    } else {
      const token =
        Cookies.get('token') !== undefined ? Cookies.get('token') : ''
      return token
    }
  },

  setPhone(vueContext, payload) {
    vueContext.commit('setPhone', payload)
  },
  setPassword(vueContext, payload) {
    vueContext.commit('setPassword', payload)
  },
  setPasswordConfirm(vueContext, payload) {
    vueContext.commit('setPasswordConfirm', payload)
  },
  setReferral(vueContext, payload) {
    vueContext.commit('setReferral', payload)
  },
  retryOtp(vueContext) {
    const response = this.$axios({
      url: vueContext.state.retryOtp.url,
      method: vueContext.state.retryOtp.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token_backup') != (null || undefined)
            ? `Bearer ${Cookies.get('token_backup')}`
            : ''
      }
    })
    return response
  },

  submitOtp(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.submitOtp.url,
      method: vueContext.state.submitOtp.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token_backup') != (null || undefined)
            ? `Bearer ${Cookies.get('token_backup')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  Submit(vueContext) {
    const phone = vueContext.state.phone.value
    const password = vueContext.state.password.value
    const password_confirmation = vueContext.state.passwordConfirm.value
    const referral = vueContext.state.referral.value

    vueContext.commit('clearErrors')
    let payload = {
      phone: phone,
      password: password,
      password_confirmation: password_confirmation,
      referral: referral
    }
    const response = this.$axios({
      url: '/register',
      method: 'POST',
      baseURL: URL.BASE,
      data: {
        ...payload
      }
    })
    vueContext.commit('unsetItem')
    return response
  }
}

export const getters = {}
