import Cookies from 'js-cookie'

const state = () => {
  return {
    shippingCode:{
      url: '/order/shipping-code/$id',
      method: 'POST',
    }
  }
}
const actions = {
  shippingCode(vueContext, payload) {
    let url = vueContext.state.shippingCode.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.shippingCode.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

}

export default {
  state,
  actions,
}
