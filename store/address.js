import Cookies from 'js-cookie'

const state = () => {
  return {
    getCity: {
      url: '/province',
      method: 'GET',
    },
    getDistricts: {
      url: '/district',
      method: 'GET',
    },
    getWards: {
      url: '/ward',
      method: 'GET',
    },
  }
}
const actions = {
  getCity(vueContext, payload) {
    const respone = this.$axios({
      url: vueContext.state.getCity.url,
      method: vueContext.state.getCity.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return respone
  },

  getDistricts(vueContext, payload) {
    const respone = this.$axios({
      url: vueContext.state.getDistricts.url,
      method: vueContext.state.getDistricts.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return respone
  },

  getWards(vueContext, payload) {
    const respone = this.$axios({
      url: vueContext.state.getWards.url,
      method: vueContext.state.getWards.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return respone
  },
}
export default {
  state,
  actions,
}
