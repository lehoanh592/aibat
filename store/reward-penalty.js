import Cookies from 'js-cookie'

const state = () => {
  return {
    getListRewardPenalty: {
      url: '/reward-penalty',
      method: 'GET',
    },
    add: {
      url: '/reward-penalty',
      method: 'POST',
    },
    edit: {
      url: '/reward-penalty/$id',
      method: 'PUT',
    },
    delete: {
      url: '/reward-penalty/$id',
      method: 'DELETE',
    },
  }
}
const actions = {
  getListRewardPenalty(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListRewardPenalty.url,
      method: vueContext.state.getListRewardPenalty.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

}

export default {
  state,
  actions,
}
