import Cookies from 'js-cookie'
import URL from '~/assets/configurations/BASE_URL';  // Import đường dẫn tới file URL.js
const state = () => {
  return {
    listConfig: {
      url: '/config',
      method: 'GET'
    },
    listConfigService: {
      url: '/config/list-service',
      method: 'GET'
    },
    addConfigService: {
      url: '/config/service',
      method: 'POST'
    },
    addOrUpdate: {
      url: '/config',
      method: 'POST'
    },
    listBusiness: {
      url: '/business',
      method: 'GET'
    },
    bank: {
      url: '/config/account-bank',
      method: 'POST'
    },
    listBanks: {
      url: '/list-bank',
      method: 'GET'
    },
    integrate: {
      url: '/integrate',
      method: 'POST'
    },
    historyIntegrate: {
      url: '/transaction',
      method: 'GET'
    },
    confirmIntegrate: {
      url: '/confirm-integrate',
      method: 'POST'
    },
    listIntegrate: {
      url: '/integrate',
      method: 'GET'
    },
    deleteIntegrate: {
      url: '/cancel-integrate',
      method: 'POST'
    },
  }
}
const actions = {
  getListBank(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.listBanks.url,
      method: vueContext.state.listBanks.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  getBank(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.bank.url,
      method: vueContext.state.bank.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  integrate(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.integrate.url,
      method: vueContext.state.integrate.method,
      baseURL: URL.INTEGRATE,
      headers: {
        'Accept' : 'application/json',
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json',
        'postman-token': '91cf6c44-3100-5905-c982-2fd7e26cb18b',
      },
      params: {
        apiKey: Cookies.get('apiKey') != (null || undefined)
          ? `${Cookies.get('apiKey')}`
          : '536c3936-b8ce-4ccf-a371-36b12712f82d'
      },
      data: payload
    })
    return response
  },
  getListIntegrate(vueContext, params) {
    const response = this.$axios({
      url: vueContext.state.listIntegrate.url,
      method: vueContext.state.listIntegrate.method,
      baseURL: URL.INTEGRATE,
      headers: {
        'accept' : 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        'postman-token': '91cf6c44-3100-5905-c982-2fd7e26cb18b'
      },
      params: {
        apiKey: Cookies.get('apiKey') != (null || undefined)
          ? `${Cookies.get('apiKey')}`
          : '536c3936-b8ce-4ccf-a371-36b12712f82d',
        ...params
      },
    })
    return response
  },
  getHistoryIntegrate(vueContext, params) {
    const response = this.$axios({
      url: vueContext.state.historyIntegrate.url,
      method: vueContext.state.historyIntegrate.method,
      baseURL: URL.INTEGRATE,
      headers: {
        'accept' : 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        'postman-token': '91cf6c44-3100-5905-c982-2fd7e26cb18b'
      },
      params: {
        apiKey: Cookies.get('apiKey') != (null || undefined)
          ? `${Cookies.get('apiKey')}`
          : '536c3936-b8ce-4ccf-a371-36b12712f82d',
        ...params
      },
    })
    return response
  },
  confirmIntegrate(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.confirmIntegrate.url,
      method: vueContext.state.confirmIntegrate.method,
      baseURL: URL.INTEGRATE,
      headers: {
        'accept' : 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        'postman-token': '91cf6c44-3100-5905-c982-2fd7e26cb18b'
      },
      params: {
        apiKey: Cookies.get('apiKey') != (null || undefined)
          ? `${Cookies.get('apiKey')}`
          : '536c3936-b8ce-4ccf-a371-36b12712f82d'
      },
      data: payload
    })
    return response
  },

  deleteIntegrate(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.deleteIntegrate.url,
      method: vueContext.state.deleteIntegrate.method,
      baseURL: URL.INTEGRATE,
      headers: {
        'accept' : 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        'postman-token': '91cf6c44-3100-5905-c982-2fd7e26cb18b'
      },
      params: {
        apiKey: Cookies.get('apiKey') != (null || undefined)
          ? `${Cookies.get('apiKey')}`
          : '536c3936-b8ce-4ccf-a371-36b12712f82d'
      },
      data: payload
    })
    return response
  },

  getListConfig(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.listConfig.url,
      method: vueContext.state.listConfig.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  getListConfigService(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.listConfigService.url,
      method: vueContext.state.listConfigService.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  getListBusiness(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.listBusiness.url,
      method: vueContext.state.listBusiness.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : `Bearer ${Cookies.get('token_backup')}`
      },
      params: {
        ...payload
      }
    })
    return response
  },

  async addOrUpdate(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.addOrUpdate.url,
      method: vueContext.state.addOrUpdate.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: payload
    })
    return response
  },

  async addConfigService(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.addConfigService.url,
      method: vueContext.state.addConfigService.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  }
}

export default {
  state,
  actions
}
