import Cookies from 'js-cookie'

const state = () => {
  return {
    getImei: {
      url: '/variant/imei/$id',
      method: 'GET',
    },
    addBatch: {
      url: '/variant/batch/$id',
      method: 'POST',
    },
    getListVariant: {
      url: '/variant',
      method: 'GET',
    },
  }
}
const actions = {
  getImei(vueContext, payload) {
    let url = vueContext.state.getImei.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.getImei.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  addBatch(vueContext, payload) {
    let url = vueContext.state.addBatch.url.toString()
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.addBatch.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getListVariant(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListVariant.url,
      method: vueContext.state.getListVariant.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
