import Cookies from 'js-cookie'

const state = () => {
  return {
    getWebsiteList: {
      url: '/theme',
      method: 'GET'
    },
    addWebsite: {
      url: '/theme',
      method: 'POST'
    },
    saveDomain: {
      url: '/theme/domain',
      method: 'POST'
    },
    getMyTheme: {
      url: '/theme/me',
      method: 'GET'
    }
  }
}
const actions = {
  saveDomain(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addWebsite.url,
      method: vueContext.state.addWebsite.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  getWebsiteList(vueContext, payload) {
    const respone = this.$axios({
      url: vueContext.state.getWebsiteList.url,
      method: vueContext.state.getWebsiteList.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return respone
  },
  getMyTheme(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getMyTheme.url,
      method: vueContext.state.getMyTheme.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  }
}

export default {
  state,
  actions
}
