import Cookies from 'js-cookie'

const state = ()=> {
  return {
    getListOrderV2: {
      url: '/order-v2',
      method: 'GET'
    },
    getListOrder: {
      url: '/order',
      method: 'GET'
    },
    getListOrder1: {
      url: '/order/variant',
      method: 'GET'
    },
    add: {
      url: '/order',
      method: 'POST'
    },
    addService: {
      url: '/order/service',
      method: 'POST'
    },
    detail: {
      url: '/order/$id',
      method: 'GET'
    },
    cancel: {
      url: '/order/cancel/$id',
      method: 'POST'
    },
    edit: {
      url: '/order/$id',
      method: 'PUT'
    },
    delete: {
      url: '/order/$id',
      method: 'DELETE'
    },
    deleteMany: {
      url: '/order/delete/many',
      method: 'DELETE'
    },
    print: {
      url: '/order/print-v2/$id',
      method: 'POST'
    },
    printA4: {
      url: '/order/print-a4/$id',
      method: 'POST'
    },
    printA5: {
      url: '/order/print-a5/$id',
      method: 'POST'
    },
    printT3: {
      url: '/print-t3/$id',
      method: 'POST'
    },
    printStamp: {
      url: '/print/temp-new/$id',
      method: 'POST'
    },
    updateStatus: {
      url: '/order/$id',
      method: 'PATCH'
    },
    payment: {
      url: '/order/payment/$id',
      method: 'POST'
    },
    ingredient: {
      url: '/order/ingredient',
      method: 'POST'
    },
    loan: {
      url: '/customer-debt',
      method: 'POST'
    },
    editLoan: {
      url: '/customer-debt/$id',
      method: 'PUT'
    },
    updateCode: {
      url: '/order/update-code/$id',
      method: 'PATCH'
    }
  }
}
const actions = {
  getListOrderV2(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListOrderV2.url,
      method: vueContext.state.getListOrderV2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  getListOrder(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListOrder.url,
      method: vueContext.state.getListOrder.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  getListOrder1(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListOrder1.url,
      method: vueContext.state.getListOrder1.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  addService(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addService.url,
      method: vueContext.state.addService.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  ingredient(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.ingredient.url,
      method: vueContext.state.ingredient.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  cancel(vueContext, payload) {
    let url = vueContext.state.cancel.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.cancel.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  deleteMany(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.deleteMany.url,
      method: vueContext.state.deleteMany.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  print(vueContext, payload) {
    let url = vueContext.state.print.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.print.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  printA4(vueContext, payload) {
    let url = vueContext.state.printA4.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printA4.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  printA5(vueContext, payload) {
    let url = vueContext.state.printA5.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printA5.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  printT3(vueContext, payload) {
    let url = vueContext.state.printT3.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printA4.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  printStamp(vueContext, payload) {
    let url = vueContext.state.printStamp.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printStamp.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  updateStatus(vueContext, payload) {
    let url = vueContext.state.updateStatus.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.updateStatus.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  payment(vueContext, payload) {
    let url = vueContext.state.payment.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.payment.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  loan(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.loan.url,
      method: vueContext.state.loan.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  editLoan(vueContext, payload) {
    let url = vueContext.state.editLoan.url
    url = url.replace('$id', payload.id)


    const response = this.$axios({
      url: url,
      method: vueContext.state.editLoan.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  updateCode(vueContext, payload) {
    let url = vueContext.state.updateCode.url
    url = url.replace('$id', payload.id)


    const response = this.$axios({
      url: url,
      method: vueContext.state.updateCode.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
}

export default {
  state,
  actions
}
