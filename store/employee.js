import Cookies from 'js-cookie'

const state = () => {
  return {
    getListEmployee: {
      url: '/employee',
      method: 'GET'
    },
    getListOrder: {
      url: '/employee/$id/order',
      method: 'GET'
    },
    add: {
      url: '/employee',
      method: 'POST'
    },
    detail: {
      url: '/employee/$id',
      method: 'GET'
    },
    edit: {
      url: '/employee/$id',
      method: 'PUT'
    },
    delete: {
      url: '/employee/$id',
      method: 'DELETE'
    },
    referral: {
      url: '/user/ref',
      method: 'GET'
    },
    addRules: {
      url: '/rules',
      method: 'POST'
    },
    getListRules: {
      url: '/rules',
      method: 'GET'
    },
    deleteRules: {
      url: '/rules/$id',
      method: 'DELETE'
    },
    wordConfig: {
      url: '/user/work-config',
      method: 'GET'
    },
    workShiftConfig:{
      url: 'user-work-shift-config/assign/$id',
      method: 'POST'
    }
  }
}
const actions = {
  getListEmployee(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListEmployee.url,
      method: vueContext.state.getListEmployee.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  addRules(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addRules.url,
      method: vueContext.state.addRules.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  getListRules(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListRules.url,
      method: vueContext.state.getListRules.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  deleteRules(vueContext, payload) {
    let url = vueContext.state.deleteRules.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.deleteRules.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },
  wordConfig(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.wordConfig.url,
      method: vueContext.state.wordConfig.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },
  workShiftConfig(vueContext, payload) {
    let url = vueContext.state.workShiftConfig.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.workShiftConfig.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  getListOrder(vueContext, payload) {
    let url = vueContext.state.getListOrder.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.getListOrder.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  getListReferral(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.referral.url,
      method: vueContext.state.referral.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      params: {
        ...payload
      }
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  },

  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : ''
      },
      data: {
        ...payload
      }
    })
    return response
  }
}

export default {
  state,
  actions
}
