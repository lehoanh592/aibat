import Cookies from 'js-cookie'

const state = () => {
  return {
    getListReceiptType: {
      url: '/receipt-type',
      method: 'GET',
    },
    printReceipt:{
      url: '/receipt-payment/receipt/$id',
      method: 'POST',
    },//thu
    printPaymentSlip:{
      url: '/receipt-payment/payment/$id',
      method: 'POST',
    }
  }
}
const actions = {
  getListReceiptType(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListReceiptType.url,
      method: vueContext.state.getListReceiptType.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  printReceipt(vueContext, payload) {
    let url = vueContext.state.printReceipt.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printReceipt.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  printPaymentSlip(vueContext, payload) {
    let url = vueContext.state.printPaymentSlip.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.printPaymentSlip.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
