import Cookies from 'js-cookie'

const state = () => {
  return {
    getListProductBarcode: {
      url: '/user/get-storage',
      method: 'GET',
    },
    generateBarcode: {
      url: '/product/generate-barcode',
      method: 'POST',
    },
    generateBarcode_v2: {
      url: '/product/generate-barcode-v2',
      method: 'POST',
    },
    addBarCode:{
      url: '/bar-code',
      method: 'POST',
    },
    addBarCode_v2:{
      url: '/bar-code/v2',
      method: 'POST',
    }

  }
}
const actions = {
  getListProductBarcode(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListProductBarcode.url,
      method: vueContext.state.getListProductBarcode.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
   async addBarCode(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.addBarCode.url,
      method: vueContext.state.addBarCode.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload, // Các dữ liệu khác

    })
    return response
  },
  async addBarCode_v2(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.addBarCode_v2.url,
      method: vueContext.state.addBarCode_v2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload, // Các dữ liệu khác

    })
    return response
  },
  async generateBarcode(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.generateBarcode.url,
      method: vueContext.state.generateBarcode.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload, // Các dữ liệu khác

    })
    return response
  },
  async generateBarcode_v2(vueContext, payload) {
    const response = await this.$axios({
      url: vueContext.state.generateBarcode_v2.url,
      method: vueContext.state.generateBarcode_v2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload, // Các dữ liệu khác

    })
    return response
  },
}

export default {
  state,
  actions,
}
