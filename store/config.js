import Cookies from 'js-cookie'

const state = () => {
  return {
    getListConfigBill: {
      url: '/config/status',
      method: 'GET',
    },
    editConfigBill:{
      url: '/config/status',
      method: 'PUT',
    },

  }
}
const actions = {
  getListConfigBill(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListConfigBill.url,
      method: vueContext.state.getListConfigBill.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  editConfigBill(vueContext, payload) {
    let url = vueContext.state.editConfigBill.url


    const response = this.$axios({
      url: url,
      method: vueContext.state.editConfigBill.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

}

export default {
  state,
  actions,
}
