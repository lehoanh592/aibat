import Cookies from 'js-cookie'

const state = () => {
  return {
    getListCustomer: {
      url: '/customer',
      method: 'GET',
    },
    add: {
      url: '/customer',
      method: 'POST',
    },
    detail: {
      url: '/customer/$id',
      method: 'GET',
    },
    edit: {
      url: '/customer/$id',
      method: 'PUT',
    },
    delete: {
      url: '/customer/$id',
      method: 'DELETE',
    },
    importCustomer: {
      url: '/customer/import',
      method: 'POST',
    },
    debt: {
      url: '/customer-debt',
      method: 'GET',
    },
    deleteDebt: {
      url: '/customer-debt/$id',
      method: 'DELETE',
    },
    repay: {
      url: '/customer-debt/repay',
      method: 'POST',
    },
    debtOld:{
      url: '/customer/debt-old/$id',
      method: 'POST',
    },
    addCustomerGroup: {
      url: '/customer-group',
      method: 'POST',
    },
    getListCustomerGroup: {
      url: '/customer-group',
      method: 'GET',
    },
    getListCustomerGroup_v2: {
      url: '/customer-group-v2',
      method: 'GET',
    },
    editCustomerGroup: {
      url: '/customer-group/$id',
      method: 'PUT',
    },
    deleteCustomerGroup: {
      url: '/customer-group/$id',
      method: 'DELETE',
    },
    customerDebtRepayHistory:{
      url: '/customer-debt/repay-history',
      method: 'GET',
    },
    uploadCustomer: {
      method: 'POST',
    },
    importCustomerV2: {
      url: '/customer/importCustomer',
      method: 'POST',
    },
    updateDebtPayment: {
      url: '/customer-debt/repay/$id',
      method: 'PUT',
    }
  }
}
const actions = {
  updateDebtPayment(vueContext, payload) {
    let url = vueContext.state.updateDebtPayment.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.updateDebtPayment.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  customerDebtRepayHistory(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.customerDebtRepayHistory.url,
      method: vueContext.state.customerDebtRepayHistory.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListCustomer(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListCustomer.url,
      method: vueContext.state.getListCustomer.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  addCustomerGroup(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addCustomerGroup.url,
      method: vueContext.state.addCustomerGroup.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  repay(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.repay.url,
      method: vueContext.state.repay.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  deleteCustomerGroup(vueContext, payload) {
    let url = vueContext.state.deleteCustomerGroup.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.deleteCustomerGroup.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  async importCustomer(vueContext, data) {
    const respone = await this.$axios({
      url: vueContext.state.importCustomer.url,
      method: vueContext.state.importCustomer.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {},
      data: data.data,
    })
    return respone
  },
  debt(vueContext, payload) {
    return this.$axios({
      url: vueContext.state.debt.url,
      method: vueContext.state.debt.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
  },
  deleteDebt(vueContext, payload) {
    let url = vueContext.state.deleteDebt.url
    url = url.replace('$id', payload)
    const response = this.$axios({
      url: url,
      method: vueContext.state.deleteDebt.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getListCustomerGroup_v2(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListCustomerGroup_v2.url,
      method: vueContext.state.getListCustomerGroup_v2.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  getListCustomerGroup(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListCustomerGroup.url,
      method: vueContext.state.getListCustomerGroup.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  editCustomerGroup(vueContext, payload) {
    let url = vueContext.state.editCustomerGroup.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.editCustomerGroup.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  debtOld(vueContext, payload) {
    let url = vueContext.state.debtOld.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.debtOld.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  async uploadCustomer(vueContext, payload) {
    const respone = await this.$axios({
      url: 'https://cdn-img.aibat.vn/api/uploads/customer',
      method: vueContext.state.uploadCustomer.method,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: payload,
    })
    return respone
  },
  importCustomerV2(vueContext, payload) {
    const response =  this.$axios({
      url: vueContext.state.importCustomerV2.url,
      method: vueContext.state.importCustomerV2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
