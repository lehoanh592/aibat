import Cookies from 'js-cookie'

const state = () => {
  return {
    getListCategoryZaloMiniApp: {
      url: '/mini-app/categories',
      method: 'GET',
    },
    add: {
      url: '/mini-app',
      method: 'POST',
    },
    getZaloMiniAppInfo: {
      url: '/mini-app/info',
      method: 'GET',
    },
  }
}
const actions = {
  getListCategoryZaloMiniApp(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListCategoryZaloMiniApp.url,
      method: vueContext.state.getListCategoryZaloMiniApp.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept' : 'application/json',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  getZaloMiniAppInfo(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getZaloMiniAppInfo.url,
      method: vueContext.state.getZaloMiniAppInfo.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : `Bearer ${Cookies.get('token_backup')}`,
      },
      params: {
        ...payload,
      },
    })
    return response
  },


}

export default {
  state,
  actions,
}
