import Cookies from 'js-cookie'

const state = () => {
  return {
    receiptPayment: {
      url: '/report/receipt-payment',
      method: 'GET',
    },
    dashboard: {
      url: '/report/dashboard',
      method: 'GET',
    },
    dashboard_v2: {
      url: '/dashboard',
      method: 'GET',
    },
    storage: {
      url: '/report/storage',
      method: 'GET',
    },
    order: {
      url: '/report/order',
      method: 'GET',
    },
    profitLoss: {
      url: '/report/profit-loss',
      method: 'GET',
    },
    aggregate: {
      url: '/report/aggregate',
      method: 'GET',
    },
    aggregateRefund: {
      url: '/report/aggregate-refund',
      method: 'GET',
    },
    reportToStaffInEachArea: {
      url: '/area/revenue',
      method: 'GET',
    },
    receivable:{
      url: '/report/debt',
      method: 'GET',
    },
    toPayDebt:{
      url:'/report/debt',
      method: 'GET'
    },
    profitAndLoss:{
      url:'/report/profit-loss',
      method: 'GET',
    },
    timekeepingReport:{
      url:'/user/work-manager',
      method:'GET',
    },
    c:{
      url:'/user/work/',
      method:'GET',
    },
    supplier:{
      url:'/report/supplier',
      method:'GET',
    },
    // customer:{
    //   url:'/report/customer',
    //   method:'GET',
    // },
    customer:{
      url:'/report/customer-v2',
      method:'GET',
    },
    task: {
      url: '/report/task',
      method: 'GET',
    },
    reportSalary: {
      url: '/user/work-all/$id',
      method: 'GET',
    },
    timekeepingReportExcel: {
      url: '/user/work-all',
      method: 'GET',
    },
    profitLossCustomer: {
      url: '/report/profit-loss-customer/$id',
      method: 'GET',
    },
    customerDebtPrintRepay: {
      url: '/customer-debt/print-repay/$id',
      method: 'GET',
    },
    timekeepingReportExcelCustomer:{
      url:'/user/work-monthly-details/$id',
      method:'GET',
    }
  }
}
const actions = {
  customerDebtPrintRepay(vueContext, payload) {
    let url = vueContext.state.customerDebtPrintRepay.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.customerDebtPrintRepay.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  profitLossCustomer(vueContext, payload) {
    let url = vueContext.state.profitLossCustomer.url
    url = url.replace('$id', payload.id)
    const response = this.$axios({
      url: url,
      method: vueContext.state.profitLossCustomer.method,
      baseURL: URL.BASE,
      headers: {
              Authorization:
                Cookies.get('token') != (null || undefined)
                  ? `Bearer ${Cookies.get('token')}`
                  : '',
            },
      params: {
        ...payload,
      },
    })
    return response
  },
  reportSalary(vueContext, payload) {
    let url = vueContext.state.reportSalary.url
    url = url.replace('$id', payload.user_id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.reportSalary.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
  dashboard(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.dashboard.url,
      method: vueContext.state.dashboard.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  dashboard_v2(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.dashboard_v2.url,
      method: vueContext.state.dashboard_v2.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  order(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.order.url,
      method: vueContext.state.order.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  storage(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.storage.url,
      method: vueContext.state.storage.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  profitAndLoss(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.profitAndLoss.url,
      method: vueContext.state.profitAndLoss.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  timekeepingReport(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.timekeepingReport.url,
      method: vueContext.state.timekeepingReport.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  timekeepingReportExcel(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.timekeepingReportExcel.url,
      method: vueContext.state.timekeepingReportExcel.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  timekeepingReportAndSalary(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.timekeepingReportAndSalary.url,
      method: vueContext.state.timekeepingReportAndSalary.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  receivable(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.receivable.url,
      method: vueContext.state.receivable.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  toPayDebt(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.toPayDebt.url,
      method: vueContext.state.toPayDebt.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  profitLoss(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.profitLoss.url,
      method: vueContext.state.profitLoss.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  receiptPayment(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.receiptPayment.url,
      method: vueContext.state.receiptPayment.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json; charset=utf-8',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  aggregate(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.aggregate.url,
      method: vueContext.state.aggregate.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  aggregateRefund(vueContext, payload) {
    const response = this.$axios ( {
      url: vueContext.state.aggregateRefund.url,
      method: vueContext.state.aggregateRefund.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin' : '*',
        Authorization:
        Cookies.get('token') !== undefined
        ? `Bearer ${Cookies.get('token')}`
          : '',
      },
      params: {
        ...payload
      }
    })
    return response;
  },
  reportToStaffInEachArea(vueContext, payload) {
    const response = this.$axios ( {
      url: vueContext.state.reportToStaffInEachArea.url,
      method: vueContext.state.reportToStaffInEachArea.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin' : '*',
        Authorization:
        Cookies.get('token') !== undefined
        ? `Bearer ${Cookies.get('token')}`
          : '',
      },
      params: {
        ...payload
      }
    })
    return response;
  },
  supplier(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.supplier.url,
      method: vueContext.state.supplier.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },

  customer(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.customer.url,
      method: vueContext.state.customer.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
  task(vueContext, payload){
    return this.$axios({
      url: vueContext.state.task.url,
      method: vueContext.state.task.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') !== undefined
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
  },
  timekeepingReportExcelCustomer(vueContext, payload) {
    let url = vueContext.state.timekeepingReportExcelCustomer.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.timekeepingReportExcelCustomer.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
