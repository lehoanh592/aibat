import Cookies from 'js-cookie'

const state = () => {
  return {
    getListRoom: {
      url: '/room-v2',
      method: 'GET',
    },
    add: {
      url: '/room',
      method: 'POST',
    },
    addMultiple: {
      url: '/room/list',
      method: 'POST',
    },
    edit: {
      url: '/room/$id',
      method: 'PUT',
    },
    detail: {
      url: '/room/$id',
      method: 'GET',
    },
    delete: {
      url: '/room/$id',
      method: 'DELETE',
    },
  }
}
const actions = {
  getListRoom(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.getListRoom.url,
      method: vueContext.state.getListRoom.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      params: {
        ...payload,
      },
    })
    return response
  },

  add(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.add.url,
      method: vueContext.state.add.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  addMultiple(vueContext, payload) {
    const response = this.$axios({
      url: vueContext.state.addMultiple.url,
      method: vueContext.state.addMultiple.method,
      baseURL: URL.BASE,
      headers: {
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  edit(vueContext, payload) {
    let url = vueContext.state.edit.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.edit.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  detail(vueContext, payload) {
    let url = vueContext.state.detail.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.detail.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },

  delete(vueContext, payload) {
    let url = vueContext.state.delete.url
    url = url.replace('$id', payload.id)

    const response = this.$axios({
      url: url,
      method: vueContext.state.delete.method,
      baseURL: URL.BASE,
      headers: {
        'Access-Control-Allow-Origin': '*',
        Authorization:
          Cookies.get('token') != (null || undefined)
            ? `Bearer ${Cookies.get('token')}`
            : '',
      },
      data: {
        ...payload,
      },
    })
    return response
  },
}

export default {
  state,
  actions,
}
