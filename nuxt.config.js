import colors from 'vuetify/es5/util/colors'
import URL from './assets/configurations/BASE_URL'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 10006, // pro
    host: '0.0.0.0' // default: localhost
  },
  head: {
    script: [
      {
        src: 'https://connect.facebook.net/en_US/sdk.js',
        async: true,
        defer: true
      },
      {
        src: `https://www.googletagmanager.com/gtag/js?id=G-1DZBXXN9G7`,
        async: true
      },
      {
        innerHTML: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-1DZBXXN9G7', { anonymize_ip: true });
      `,
        type: 'text/javascript',
        charset: 'utf-8'
      },
      {
        innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K84LP5LP');`,
        type: 'text/javascript',
        charset: 'utf-8'
      }
    ],
    noscript: [
      {
        innerHTML: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K84LP5LP"
        height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
        body: true
      }
    ],
    __dangerouslyDisableSanitizers: ['script'],
    titleTemplate: '',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, user-scalable=no'
      },
      {
        name: 'description',
        content: 'Aibat - Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn. Giúp bạn quản lý bán hàng và kinh doanh từ online đến cửa hàng và quản lý tập trung.'
      },
      {
        name: 'keywords',
        content: 'phan mem quan ly ban hang, aibat, phan mem aibat, phan mem quan ly kinh doanh, phan mem quan ly aibat'
      },
      {
        name: 'content',
        content: 'Aibat - Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn. Giúp bạn quản lý bán hàng và kinh doanh từ online đến cửa hàng và quản lý tập trung.'
      },
      {property: 'og:title', content: 'Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'},
      {
        property: 'og:description',
        content: 'Aibat - Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn. Giúp bạn quản lý bán hàng và kinh doanh từ online đến cửa hàng và quản lý tập trung.'
      },
      {property: 'og:image', content: 'https://web.aibat.vn/LogoAibat.png'}, // URL to your preview image
      {property: 'og:logo', content: 'https://web.aibat.vn/LogoAibat.png'}, // URL to your preview image
      {property: 'og:url', content: 'https://web.aibat.vn'},
      {property: 'og:type', content: 'website'},
      {property: 'og:image:secure_url', content: 'https://support.aibat.vn/wp-content/uploads/2024/05/banner-ads.jpg'},
      {
        property: 'og:site_name',
        content: 'Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'
      },
      {
        property: 'og:image:alt',
        content: 'Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'
      },
      {
        property: 'twitter:card',
        content: 'Aibat - Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn. Giúp bạn quản lý bán hàng và kinh doanh từ online đến cửa hàng và quản lý tập trung.'
      },
      {
        property: 'twitter:site',
        content: '@Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'
      },
      {
        property: 'twitter:creator',
        content: 'Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'
      },
      {property: 'twitter:url', content: 'https://web.aibat.vn'},
      {
        property: 'twitter:title',
        content: 'Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn | Aibat'
      },
      {
        property: 'twitter:description',
        content: 'Aibat - Phần mềm quản lý bán hàng và kinh doanh trên di động tốt nhất cho bạn. Giúp bạn quản lý bán hàng và kinh doanh từ online đến cửa hàng và quản lý tập trung.'
      },
      {property: 'twitter:image', content: 'https://ibb.co/Bs9YTYG'},
      {property: 'twitter:image:width', content: '500'},
      {property: 'twitter:image:height', content: '300'},
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/LogoAibat.png'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/main.scss',
    '~/assets/font-size.scss',
    '~/assets/data_table_base.scss',
    '~/assets/font.scss',
    '~/assets/css/materialdesignicons.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~/plugins/PersistedState.js', ssr: false},
    '~/plugins/axios.js',
    '~/plugins/globalComponent.js',
    '~/plugins/APIs.js',
    '~/plugins/ult.js',
    '~/plugins/ultilities.js',
    '@/plugins/chart.js',
    '~/plugins/vuetify.js',
    {src: '~/plugins/toastification.js', ssr: false},
    {
      src: '~/plugins/notification.js',
      ssr: false
    },
    {src: '~/plugins/common.js', ssr: false}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    'nuxt-socket-io',
  ],
  io: {
    // module options
    sockets: [{
      name: 'user',
      url: URL.SOCKET,
      transports: ['websocket'],
      default: true,
    }]
  },
  router: {
    middleware: ['mobileRedirect', 'auth', 'redirect', 'checkBusinessId'],
    '/*': {cors: true},
    serverMiddleware: {
      // path: '/api', handler: '~/middleware/pusher-auth.js'
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: URL.BASE
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.lightBlue.lighten1,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#3ba8e7',
          primary2: '#1d50a3',
          success: '#3ba8e7',
          successNotify: '#4caf50',
          warning: colors.amber.base,
          info: colors.teal.lighten1
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, {isDev, isClient}) {

      config.node = {
        fs: 'empty'
      }

      // ....
    }
  }
}
