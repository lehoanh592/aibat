// plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import '~/assets/css/materialdesignicons.min.css' // Import the icon font from your preferred CDN

Vue.use(Vuetify)

export default (ctx) => {
  const vuetify = new Vuetify({
    // Add any other Vuetify configuration options here
    icons: {
      iconfont: 'mdi', // Use the Material Design Icons font
    },
  })

  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
}
