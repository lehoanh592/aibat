// Trong một file util hoặc service, ví dụ logger.js
const fs = require('fs');
const path = require('path');

// Đường dẫn tới tệp log
const logFilePath = path.resolve(__dirname, 'app.log');

// Hàm ghi log
function logToFile(logMessage) {
  const timestamp = new Date().toISOString();
  const formattedMessage = `${timestamp}: ${logMessage}\n`;

  fs.appendFile(logFilePath, formattedMessage, (err) => {
    if (err) {
      console.error('Error writing to log file:', err);
    }
  });
}

// Export hàm để sử dụng ở nơi khác trong ứng dụng
module.exports = { logToFile };
