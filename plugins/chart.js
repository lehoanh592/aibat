import Vue from 'vue'
import {Bar, Line} from 'vue-chartjs'

Vue.component('BarChart', {
  extends: Bar,
  props: {
    data: {
      type: Object,
      required: true,
    },
    options: {
      type: Object,
      required: false,
      default: () => ({
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            barPercentage: 0.4
          }],
          yAxes: [{
            display: true,
            stacked: true,
            ticks: {
              min: 0,
            }
          }]
        }
        // legend: {
        //   display: false,
        // },
      }),
    },

  },
  watch: {
    data(value) {
      this.renderChart(value, this.options)
    },
  },
  mounted() {
    this.renderChart(this.data, this.options)
  },
})


Vue.component('LineChart', {
  extends: Line,
  props: {
    data: {
      type: Object,
      required: true,
    },
    options: {
      type: Object,
      required: false,
      default: () => ({
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            display: true,
            stacked: true,
            ticks: {
              min: 0,
            }
          }]
        }
        // legend: {
        //   display: false,
        // },
      }),
    },

  },
  watch: {
    data(value) {
      this.renderChart(value, this.options)
    },
  },
  mounted() {
    this.renderChart(this.data, this.options)
  },
})
