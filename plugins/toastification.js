// plugins/toastification.js

import Vue from 'vue'
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'

// Optionally configure the Toast component
const options = {
  // Configure your options here
  position: 'top-right',
  timeout: 5000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: 'button',
  icon: true,
  rtl: false
}

Vue.use(Toast, options)
