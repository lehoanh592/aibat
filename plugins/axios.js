import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config'

// Axios interceptor. Auto log out when token is expired or invalid
export default function (context) {
  context.$axios.onResponse((response) => {
    if (typeof response.data === 'string') {
      return {
        error: false,
        data: response.data
      }
    }

    const status = response.data.status
    if (status === 200 || response.status === 200) {
      return {
        error: false,
        data: response.data
      }
    } else {
      context.app.$showWarnNotify(context.app.router.app, 'Có lỗi xảy ra, vui lòng thử lại')
    }
  })

  context.$axios.onError((error) => {
    if (error.response.status === 422) {
      return {
        error: true,
        status_http: 422,
        data: error.response.data
      }
    }

    if (error.response.status === 403) {
      return {
        error: true,
        status_http: 403,
        data: error.response.data
      }
    }

    if (error.response.status === 401) {
      let path = context.route.path;
      if (path === '/dang-nhap' || path === '/dang-ky') {
        return {
          error: true,
          status_http: 401,
          data: error.response.data
        }
      } else {
        setTimeout(() => {
          Object.keys(Cookies.get()).forEach(function (cookieName) {
            Cookies.remove(cookieName);
          });
          context.redirect(path);
        }, 1000);
      }
    }

    context.app.router.app.$log.error('Có lỗi/exception: ', {
      error,
      response: error.response
    })

    context.app.$showError(
      context.app.router.app,
      'Có lỗi xảy ra khi kết nối đến server'
    )

    return {
      error: true
    }
  })
}
