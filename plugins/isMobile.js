// plugins/deviceDetection.js
import Vue from 'vue';
import isMobile from 'ismobilejs';

function detectAndroidOriOS() {
  const userAgent = navigator.userAgent;

  if (/Android/i.test(userAgent)) {
    return 'Android';
  } else if (/iPhone|iPad|iPod/i.test(userAgent)) {
    return 'iOS';
  } else {
    return 'Other';
  }
}

Vue.prototype.$deviceType = detectAndroidOriOS;
Vue.prototype.$isMobile = isMobile;
