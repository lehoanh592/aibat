// import * as Cookies from 'js-cookie'
import URL from '~/assets/configurations/BASE_URL.js'
import {parseInt} from "lodash/string.js";

export default function (context, inject) {
  inject('redirect', (payload) => {
    if (payload.samepage) {
      context.app.router.replace({path: payload.url})
    } else {
      window.location.href = payload.url
    }
  }),
    inject('genTransId', () => {
      return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
          c ^
          (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
      )
    })
  inject('emitSocketNotification', (payload) => {
    try {
      context.app.$nuxtSocket({
        name: 'user',
      }).emit('user', {user_id: parseInt(payload.data)});

    } catch (e) {
    }
  })
  inject('formatMoney', (payload) => {
    try {
      return payload == null || payload.amount == null
        ? ''
        : payload.amount
          .toString()
          .replace(/\D/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    } catch (e) {
    }
  })
  inject('preventBackspaceVAutocomplete', (ref, context) => {//block backspace in autocomplete when there's no search input fhung
    try {
      context.$nextTick(() => {
        const autocompleteEl = context.$refs[ref];
        if (!autocompleteEl) return;

        const input = autocompleteEl.$el.querySelector('input');
        if (input) {
          const handler = (e) => {
            if ((e.key === 'Backspace' || e.key === 'Delete') && !e.target.value) {
              e.stopPropagation();
              e.preventDefault();
            }
          };
          input.addEventListener('keydown', handler, true);
        }
      });
    } catch (e) {
    }
  })
  inject('formatNegativeMoney', (payload) => {
    try {
      if (payload == null || payload.amount == null) return '';
      let amountStr = payload.amount.toString();
      let isNegative = amountStr.startsWith('-');
      if (isNegative) {
        amountStr = amountStr.substring(1);
      }
      amountStr = amountStr.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return isNegative ? `-${amountStr}` : amountStr;
    } catch (e) {
      return '';
    }
  });

  inject('formatMoneyExport', (payload) => {
    try {
      return payload == null || payload.amount == null
        ? ''
        : payload.amount
          .toString()
          .replace(/\D/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    } catch (e) {
    }
  })

  inject('setCookie', (cname, cvalue, exdays) => {
    var d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    var expires = 'expires=' + d.toUTCString()
  })
  inject('getCookie', (cname) => {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  })
  inject('isEmail', (str) => {
    let email =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return email.test(str)
  })
  inject('checkValuePrice', (str) => {
    if (str && typeof str === 'string' && str.includes('.')) {
      return true
    } else {
      return false
    }
  })
  inject('replaceMiddleWithStars', (str) => {
    if (str === 'NaN') {
      str = '0'
    }
    if (str.includes('.')) {
      str = str.replaceAll('.', '')
    }

    if (str.length <= 3) {
      const middleIndex = Math.floor(str.length / 2)
      return str.substring(0, middleIndex) + '*' + str.substring(middleIndex + 1)
    } else {
      const middleCount = Math.min(3, Math.floor(str.length / 2))
      const middlePart = '*'.repeat(middleCount)
      const start = Math.floor((str.length - middleCount) / 2)
      return str.substring(0, start) + middlePart + str.substring(start + middleCount)
    }
  })
  inject('replaceAllWithStars', (str) => {
    if (str === 'NaN') {
      str = '0'
    }
    if (str.includes('.')) {
      str = str.replaceAll('.', '')
    }

    // Replace all digits with '*'
    str = str.replace(/\d/g, '*')

    if (str.length <= 3) {
      const middleIndex = Math.floor(str.length / 2)
      return str.substring(0, middleIndex) + '*' + str.substring(middleIndex + 1)
    } else {
      const middleCount = Math.min(3, Math.floor(str.length / 2))
      const middlePart = '*'.repeat(middleCount)
      const start = Math.floor((str.length - middleCount) / 2)
      return str.substring(0, start) + middlePart + str.substring(start + middleCount)
    }
  })
  inject('isNullOrEmpty', (str) => {
    return str === null || str === undefined || str === '' || str.length === 0
  })
  inject('htmlentities', (str) => {
    return str.replace(/[&<>"']/g, function (match) {
      switch (match) {
        case '&':
          return '&amp;'
        case '<':
          return '&lt;'
        case '>':
          return '&gt;'
        case '"':
          return '&quot;'
        case '\'':
          return '&#39;'
        default:
          return match
      }
    })
  })

  inject('createSlug', (string) => {
    const search = [
      /(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g,
      /(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g,
      /(ì|í|ị|ỉ|ĩ)/g,
      /(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g,
      /(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g,
      /(ỳ|ý|ỵ|ỷ|ỹ)/g,
      /(đ)/g,
      /(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/g,
      /(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/g,
      /(Ì|Í|Ị|Ỉ|Ĩ)/g,
      /(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/g,
      /(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/g,
      /(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/g,
      /(Đ)/g,
      /[^a-zA-Z0-9\-_]/g
    ]

    const replace = [
      'a',
      'e',
      'i',
      'o',
      'u',
      'y',
      'd',
      'A',
      'E',
      'I',
      'O',
      'U',
      'Y',
      'D',
      '-'
    ]

    let result = string

    for (let i = 0; i < search.length; i++) {
      result = result.replace(search[i], replace[i])
    }

    result = result.replace(/(-)+/g, '-')
    result = result.toLowerCase()

    return result
  })

  inject('isMobilePhone', (str) => {
    let phoneNot84 = /[0]{1}[35789]{1}[0-9]{8}$/
    let phone84 = /^[84]{2}[35789]{1}[0-9]{8}$/
    return phoneNot84.test(str) || phone84.test(str)
  })

  inject('isEmail', (str) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(str).toLowerCase());
  })

  inject('convertDateTime', (dateTime) => {
    const date = new Date(dateTime)
    // Extract date and time components
    const day = date.getDate()
    const month = date.getMonth() + 1 // Months are 0-based, so add 1
    const year = date.getFullYear()
    const hours = date.getHours()
    const minutes = date.getMinutes()
    const seconds = date.getSeconds()

    // Format the components into "dd/mm/yyyy h:i:s" format
    const formattedDate = `${day.toString().padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}`
    const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`

    return `${formattedDate} ${formattedTime}`
  })

  inject('getUrlImage', (url) => {
    if (typeof url === 'string' && (url.includes('http://') || url.includes('https://'))) {
      return url
    } else {
      return URL.IMAGE + url
    }
  })

  inject('getNumberInArray', (businessId) => {
    let stt = 0
    if (businessId <= 7) {
      stt = businessId - 1
    } else if (businessId >= 9 && businessId <= 12) {
      stt = businessId - 2
    } else if (businessId > 12 && businessId <= 17) {
      stt = 11
    } else {
      stt = businessId - 7
    }
    return stt
  })

  inject('getTimeConfigService', (startDate, endDate, type) => {
    const start = new Date(startDate)
    const end = new Date(endDate)

    // Tính khoảng thời gian giữa hai ngày
    const timeDifference = end - start

    // Tính theo phút
    if (type === 1) {
      return Math.round(timeDifference / (1000 * 60))
    }

    // Tính theo giờ
    if (type === 2) {
      let hourOld = timeDifference / (1000 * 60 * 60)
      let hourNew = 0
      if (hourOld % 1 === 0) {
        hourNew = Math.floor(hourOld)
      } else {
        hourNew = Math.floor(hourOld) + 1
      }
      return hourNew
    }

    // Tính theo ngày
    if (type === 3) {
      let dayOld = timeDifference / (1000 * 60 * 60 * 24)
      let dayNew = 0
      if (dayOld % 1 === 0) {
        dayNew = dayOld
      } else {
        dayNew = Math.floor(dayOld) + 1
      }
      return dayNew
    }

    // Tính theo tháng
    if (type === 4) {
      let day = timeDifference / (1000 * 60 * 60 * 24)
      let month = 0
      if (day % 30 === 0) {
        month = Math.floor(day / 30)
      } else {
        month = Math.floor(day / 30) + 1
      }

      return month
    }

    // Nếu type không hợp lệ
    return null
  })

  inject('getPriceDebt', (retailCost, orderPayment) => {
    let price = 0
    for (const value of orderPayment) {
      price += value.price
    }
    return retailCost - price
  })

  inject('getPricePayment', (orderPayment) => {
    let price = 0
    for (const value of orderPayment) {
      price += value.price
    }
    return price
  })

  inject('getPriceOrderServiceFee', (orderServiceFee) => {
    let price = 0
    for (const value of orderServiceFee) {
      price += value.price
    }
    return price
  })

  inject('isHasSpecial', (str) => {
    let sp = '~;+@#$%^&*(){} |=-\''
    let strLower = str.toLowerCase()
    let isOK = false
    for (let i = 0; i < strLower.length; i++) {
      if (sp.indexOf(strLower[i]) >= 0) {
        isOK = true
        break
      }
    }
    return isOK
  })

  inject('isHasSpecialll', (str) => {
    let sp = '#&[]{}'
    let strLower = str.toLowerCase()
    let isOK = false
    for (let i = 0; i < strLower.length; i++) {
      if (sp.indexOf(strLower[i]) >= 0) {
        isOK = true
        break
      }
    }
    return isOK
  })

  inject('isHasVietnamese', (str) => {
    let vn =
      'àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ'
    let strLower = str.toLowerCase()

    let isOK = false
    for (let i = 0; i < strLower.length; i++) {
      if (vn.indexOf(strLower[i]) >= 0) {
        isOK = true
        break
      }
    }
    return isOK
  })

  inject('nonAccentVietnamese', (str) => {
    str = str.toLowerCase()
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    // Some system encode vietnamese combining accent as individual utf-8 characters
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, '') // Huyền sắc hỏi ngã nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, '') // Â, Ê, Ă, Ơ, Ư
    str = str.replace(/\W/g, '-') // kí tự đặc biệt

    return str
  })

  inject('getFileBlobUrl', (file, setBlobUrl) => {
    // var reader = new FileReader()
    // reader.addEventListener('load', () => {
    //   setBlobUrl(reader.result)
    // })
    // if (file) {
    //   reader.readAsDataURL(file)
    // }
    if (file) {
      var url = URL.createObjectURL(file)
      setBlobUrl(url)
    }
  })
  inject('formatFloat', (val, decimalPlace) => {
    if (!val || isNaN(val)) {
      return 0;
    }
    return parseFloat(parseFloat(val).toFixed(decimalPlace))
  })
  inject('removePeriod', (val) => {
    return String(val).replaceAll('.', '')
  })
  inject('countDecimal', (val) => {
    if (Math.floor(val) != val && val) {
      const decimalPart = val.toString().split(/[.,]/)[1];
      return decimalPart ? decimalPart.length : 0;
    }
    return 0;
  })
}
