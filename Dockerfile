# Use the official Node.js 14.19.1-alpine base image
FROM node:14.19.1-alpine

# Create destination directory
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

# Copy the app, note .dockerignore
COPY . /usr/src/nuxt-app/
LABEL name="aibat_web" version="1.0"

# Install dependencies
RUN npm install

# Install pm2 globally
RUN npm install pm2 -g

# Expose the port on which your Nuxt.js app will run
EXPOSE 10006

# Set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# Set app port (optional, as NUXT_HOST is already set to 0.0.0.0)
# ENV NUXT_PORT=10006

# Start the app with pm2
CMD ["pm2-runtime", "npm", "--", "start"]
