// middleware/redirect.js

export default function ({ route, redirect }) {
  // Check if the user is accessing the root path
  if (route.path === '/') {
    // Redirect to the dashboard path
    redirect('/dashboard');
  }
}
