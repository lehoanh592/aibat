import ULT from '~/plugins/ult'
import Cookies from 'js-cookie'

export default function(context) {
  if (context.route.path === '/chon-nganh-hang') {
    let businessId = Cookies.get('businessId')
    if (businessId) {
      context.redirect('/dashboard')
    }
  }
}
