import ULT from '~/plugins/ult'
import Cookies from "js-cookie";

export default function (context) {
  if (context.route.path === '/dang-nhap') {
    context.store.dispatch('login/isAuthenticated', context.req).then((res) => {
      if (res.length !== 0) {
        context.redirect('/dashboard')
      }
    })
  } else if (context.route.path === '/dang-ky') {
    context.store.dispatch('login/isAuthenticated', context.req).then((res) => {
      if (res.length !== 0) {
        context.redirect('/dashboard')
      }
    })
  } else if (context.route.path === '/chon-nganh-hang') {
    if (!Cookies.get('token_backup')) {
      context.redirect('/dashboard')
    }
  } else {
    context.store
      .dispatch('login/isAuthenticated', context.req)
      .then((res) => {
        if (res.length === 0 || res.length === undefined) {
          context.redirect('/dang-nhap')
        }
      })
      .catch((e) => {
        context.redirect('/dang-nhap')
      })
  }
}
