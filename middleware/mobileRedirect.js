// middleware/mobileRedirect.js

export default function ({req, redirect}) {
  if (process.server) {
    const userAgent = req.headers['user-agent'];

    // Check if the request is from a mobile device
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
      // Check if it's an iOS device
      if (/iPhone|iPad|iPod/i.test(userAgent)) {
        return redirect('https://apps.apple.com/vn/app/aibat/id6467622531');
      }

      // Check if it's an Android device
      if (/Android/i.test(userAgent)) {
        return redirect('https://play.google.com/store/apps/details?id=com.aibat.app');
      }
    }
  }
}
