import Cookies from "js-cookie";

export default function (context) {
  if (context.route.path === '/dashboard') {//check if user change their route to /dashboard, we will call api to get new using_days
    if (Cookies.get('token') !== undefined) {
      context.store
        .dispatch('user/userInfo', context.req)
        .then((res) => {
          if (res.length === 0 || res) {
            if (res.data.data.using_days !== undefined) {
              Cookies.remove('using_days')
              Cookies.set('using_days', res.data.data.using_days, {expires: 24 * 30 * 12})
            }
            if (res.data.data.user_active_code !== undefined) {
              Cookies.remove('userActiveCode')
              Cookies.set('userActiveCode', res.data.data.user_active_code, {expires: 24 * 30 * 12})
            }
          }
        })

    }
  }
}
