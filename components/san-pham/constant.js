export const attributeType = {
  color: 'Màu sắc',
  size: 'Kích thước',
  material: 'Chất liệu',
  service: 'Dịch vụ'
}
export const attributeTypeOptions = Object.values(attributeType).map(
  (attributeType) => ({text: attributeType, value: attributeType})
)

export const exchangeType = {
  box: 'Hộp',
  bin: 'Thùng',
  package: 'Gói',
  roll:'Cuộn',
  wire:'Dây',
  coil:'Lốc',
  kg: 'Kg',
  tree : 'Cây',
}
export const exchangeOptions = Object.values(exchangeType).map(
  (exchangeType) => ({text: exchangeType, value: exchangeType})
)
