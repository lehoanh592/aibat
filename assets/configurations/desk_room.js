let eventGuid = 0
let todayStr = new Date().toISOString().replace(/T.*$/, '') // YYYY-MM-DD of today

export const INITIAL_EVENTS = [
  {
    id: createEventId(),
    title: "dddddd",
    start: new Date("2024-09-20 07:00:00"),
    end: new Date("2024-09-20 09:00:00"),
    allDay: false
  },

]

export function createEventId() {
  return String(eventGuid++)
}
