const URL = {
  IMAGE: 'https://cdn-img.aibat.vn/api/',
  BASE: 'https://api.aibat.vn/api/',
  SOCKET: 'https://socket.aibat.vn',
  INTEGRATE: 'https://integrate-bank.aibat.vn',
}
export default URL
