const ConstantsValue = {
  Business: [
    [//bi-a
      {
        headerRoom: 'Quản lý bàn',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm mặt hàng',
        buttonUploadProduct: 'Upload mặt hàng',
        buttonAddRoom: 'Thêm bàn',
        buttonBook: 'Đặt bàn',
        labelProductRoom: 'Chọn món',
        labelProductBuy: 'Chọn hàng hoá',
        headerConfigService: 'Cài đặt giá cho bàn',
        headerRoomPopup: 'bàn',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Doanh thu bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Hoá đơn bán hàng',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//nha hang, quan an
      {
        headerRoom: 'Quản lý phòng/bàn',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm hàng hoá',
        buttonUploadProduct: 'Upload hàng hoá',
        buttonAddRoom: 'Thêm phòng/bàn',
        buttonBook: 'Đặt phòng/bàn',
        labelProductRoom: 'Chọn thực đơn/dịch vụ',
        labelProductBuy: 'Chọn hàng hoá',
        headerConfigService: 'Cài đặt giá cho phòng/bàn',
        headerRoomPopup: 'phòng/bàn',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Doanh thu bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Hoá đơn bán hàng',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//karaoke
      {
        headerRoom: 'Quản lý phòng',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm mặt hàng',
        buttonUploadProduct: 'Upload mặt hàng',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn hàng hoá',
        labelProductRoom: 'Chọn sản phẩm/dịch vụ',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Doanh thu bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Hoá đơn bán hàng',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//nha nghi, homestay
      {
        headerRoom: 'Quản lý phòng',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm mặt hàng',
        buttonUploadProduct: 'Upload mặt hàng',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn hàng hoá',
        labelProductRoom: 'Chọn mặt hàng/dịch vụ',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Doanh thu phòng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Thanh toán',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//khách sạn
      {
        headerRoom: 'Quản lý phòng',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm mặt hàng',
        buttonUploadProduct: 'Upload mặt hàng',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn hàng hoá',
        labelProductRoom: 'Chọn mặt hàng/dịch vụ',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo doanh thu',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Thanh toán',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn mua hàng'
      }
    ],
    [//cửa hàng điện thoại
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//quan ly cua hang
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Quản lý phiếu thu chi',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//quan ly xuong san xuat
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo doanh thu',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn xuất kho',
        headerBuy: 'Quản lý đơn nhập kho',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderBuy: 'Tạo phiếu nhập kho',
        headerOrderSell: 'Tạo phiếu xuất kho'
      }
    ],
    [//so hoa doanh nghiep
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReport: 'Báo cáo doanh thu',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn xuất kho',
        headerBuy: 'Quản lý đơn nhập kho',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderBuy: 'Tạo phiếu nhập kho',
        headerOrderSell: 'Tạo phiếu xuất kho'
      }
    ],
    [//cua hang tap hoa
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//quan cafe, do uong
      {
        headerRoom: 'Quản lý bàn',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm hàng hoá',
        buttonUploadProduct: 'Upload hàng hoá',
        buttonAddRoom: 'Thêm bàn',
        buttonBook: 'Đặt bàn',
        labelProductRoom: 'Chọn món',
        labelProductBuy: 'Chọn hàng hoá',
        headerConfigService: 'Cài đặt giá cho bàn',
        headerRoomPopup: 'bàn',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Hoá đơn bán hàng',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Quản lý phiếu thu chi',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn mua hàng'
      }
    ],
    [//sieu thi
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//nail
      {
        headerRoom: 'Phòng',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm/dịch vụ',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Quản lý phiếu thu chi',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//kinh doanh khac
      {
        headerRoom: null,
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm sản phẩm',
        buttonUploadProduct: 'Upload sản phẩm',
        buttonAddRoom: 'Thêm phòng',
        buttonBook: 'Đặt phòng',
        labelProductBuy: 'Chọn sản phẩm',
        labelProductRoom: 'Chọn sản phẩm',
        headerConfigService: 'Cài đặt giá cho phòng',
        headerRoomPopup: 'phòng',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Báo cáo bán hàng',
        headerReportSell: 'Báo cáo nhập hàng',
        headerReportProfit: 'Báo cáo lãi lỗ',
        headerSell: 'Quản lý đơn bán hàng',
        headerBuy: 'Quản lý đơn nhập hàng',
        headerReceiptPayment: 'Quản lý phiếu thu chi',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
    [//san bong
      {
        headerRoom: 'Quản lý sân',
        headerPayment: 'hoá đơn',
        labelSearch: 'Enter để tìm kiếm',
        buttonAddProduct: 'Thêm mặt hàng',
        buttonUploadProduct: 'Upload mặt hàng',
        buttonAddRoom: 'Thêm sân',
        buttonBook: 'Đặt sân',
        labelProductRoom: 'Chọn dịch vụ',
        labelProductBuy: 'Chọn hàng hoá',
        headerConfigService: 'Cài đặt giá cho sân',
        headerRoomPopup: 'sân',
        headerReportSupplier: 'Báo cáo nhà cung cấp',
        headerReportCustomer: 'Thống kê doanh số mua hàng',
        headerReport: 'Doanh thu bán hàng',
        headerReportSell: 'Báo cáo mua hàng',
        headerReportProfit: 'Báo cáo lợi nhuận',
        headerSell: 'Hoá đơn bán hàng',
        headerBuy: 'Hoá đơn mua hàng',
        headerReceiptPayment: 'Tổng quan sổ quỹ',
        headerOrderSell: 'Tạo đơn bán hàng',
        headerOrderBuy: 'Tạo đơn nhập hàng'
      }
    ],
  ],
  CheckShowHide: [
    [ //bia
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: true,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//nha hang,quan an
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: false,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//karaoke
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: true,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//homestay, nha nghi
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: true,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//khách sạn
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: true,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//cửa hàng điện thoại
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//quan ly ban hang
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//quan ly xuong san xuat
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//so hoa doanh nghiep
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//cua hang tap hoa
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//quan cafe, do uong
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: false,
        isProduct: false,
        typeProduct: 2
      }
    ],
    [//sieu thi
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//nail
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [//kinh doanh khac
      {
        showUploadExcel: true,
        showDownloadExcel: true,
        showWholesaleCost: true,
        showConfigService: false,
        isProduct: true,
        typeProduct: 1
      }
    ],
    [ //san bong
      {
        showUploadExcel: false,
        showDownloadExcel: false,
        showWholesaleCost: false,
        showConfigService: true,
        isProduct: false,
        typeProduct: 2
      }
    ],
  ],
  listUserShow: [
    //product
    {
      id: 44,
      show: true
    },
    {
      id: 6,
      show: true
    },
    {
      id: 181,
      show: true
    },

    //dev
    {
      id: 12,
      show: true
    }
  ],
  businessTable: [1, 2, 3, 4, 5, 12, 21],
  otherFeeException: [11,17 ], //sieu thi, tap hoa
}

export default ConstantsValue
