const today = new Date()
today.setHours(0, 0, 0, 0)

const yesterday = new Date()
yesterday.setDate(today.getDate() - 1)
yesterday.setHours(0, 0, 0, 0)
const sevenDaysAgo = new Date()
sevenDaysAgo.setDate(today.getDate() - 7)
sevenDaysAgo.setHours(0, 0, 0, 0)
const oneMonthAgo = new Date()
oneMonthAgo.setMonth(today.getMonth() - 1)
oneMonthAgo.setHours(0, 0, 0, 0)
const threeMonthsAgo = new Date()
threeMonthsAgo.setMonth(today.getMonth() - 3)
threeMonthsAgo.setHours(0, 0, 0, 0)
const aYearAgo = new Date()
aYearAgo.setFullYear(today.getFullYear() - 1)
aYearAgo.setHours(0, 0, 0, 0)
const Locale= {
  vi: [
    {
      direction: 'ltr',
      format: 'dd-mm-yyyy',
      separator: ' ~ ',
      applyLabel: 'Xác nhận',
      cancelLabel: 'Huỷ',
      weekLabel: 'W',
      customRangeLabel: 'Khoảng tuỳ chỉnh',
      daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      monthNames: ['T.Một', 'T.Hai', 'T.Ba', 'T.Bốn', 'T.Năm', 'T.Sáu', 'T.Bảy', 'T.Tám', 'T.Chín', 'T.Mười', 'T.Mười một', 'T.Mười hai'],
      firstDay: 0
    }
  ],
  en: [
    {
      direction: 'ltr',
      format: 'mm-dd-yyyy',
      separator: ' ~ ',
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
      weekLabel: 'W',
      customRangeLabel: 'Custom Range',
      daysOfWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      firstDay: 0
    }
  ],
  dateRange: {
    "Hôm nay": [today, today],
    "Hôm qua": [yesterday, yesterday],
    "7 ngày trước": [sevenDaysAgo, today],
    "1 tháng trước": [oneMonthAgo, today],
    "3 tháng trước": [threeMonthsAgo, today],
    "1 năm trước": [aYearAgo, today]

  }
}
export default Locale;
